<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Log;
use App\Pete;
use App\Site;
use App\Option;
use App\PeteOption;
use DateTime;
use App\Standard\Validator;
use App\Standard\License;
use Illuminate\Support\Facades\Route;

class AdIndex
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	
    public function __construct(Guard $auth)
    {
      
    }
	
    public function handle($request, Closure $next)
    {
		$route = Route::getRoutes()->match($request);
		$action = $route->getAction();
		$action = explode('@',$action['controller']);
		Log::info("Action: $action[1]");
		
		
		if(($action[1] != "index") & ($action[1] != "edit") & ($action[1] != "reload_server") & ($action[1] != "register_your_license") & ($action[1] != "pete_converter") & ($action[1] != "getLogin") & ($action[1] != "postLogin") & ($action[1] != "pete_converter_export") & ($action[1] != "update_license") & ($action[1] != "export") ){
			
			$pete_options = new PeteOption();
			$validation_api_key = $pete_options->get_meta_value('validation_api_key');
			$validation_user_email = $pete_options->get_meta_value('validation_user_email');
			$token = $pete_options->get_meta_value('token');
			$created_at_string = $pete_options->get_meta_value('created_at_string');
	
			$validation_event = new DateTime($pete_options->get_meta_date('created_at_string')); 
				
			$license = new License($validation_user_email, $created_at_string,"WordpressPete",$token,"wordpresspete.com");
			$validator = new Validator();
			if($validator->validate($license, $validation_api_key)) {
				$validate = "YES";
			} else {
				$validate = "NO";
			} 
			
			if(Site::count() >= 2){
			
				$current_time = new DateTime('');
				$validation_event = new DateTime($pete_options->get_meta_date('validation_event')); 
		
				if(($validate == "NO") || ($current_time > $validation_event)){
					return redirect('/'.'register'.'_'.'your'.'_'.'license');
				}
		
			}else{
				
				if(($validate == "NO") & ($action[1] == "wordpress_plus_laravel")){
					return redirect('/'.'register'.'_'.'your'.'_'.'license');
				}
			}
		
		}
		
		
		return $next($request);
    }
}
