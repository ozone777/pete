<?php

namespace App\Http\Middleware;
use Log;
use Closure;
use ErrorException;

class AdMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
		$route = $request->route();
        // Perform action
		$action = app('request')->route()->getAction();
		
		$value = $request->session()->pull('os', 'default');
		
		try {
		
		Log::info($action['as']);
		Log::info($value);
		
		if(($action['as'] == "sites.show") && ($value == "centos7")){
		  Log::info("entro en condicion");
		  exec('sudo /bin/systemctl restart httpd.service');
		}
		
		$request->session()->forget('os');

		return $response;	
		}

		//catch exception
		catch(ErrorException $exception) {
  		Log::info('error el middleware');
		
  	    return $response;
		}
		
	}
		
}