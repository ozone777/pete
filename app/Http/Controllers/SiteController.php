<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use App\Site;
use App\Adomain;
use App\Secure;
use App\User;
use App\Backup;
use Input;
use Response;
use App\App;
use Validator;
use App\Option;
use DB;
use App\Pete;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Redirect;
use App\PeteOption;
use Illuminate\Routing\Route;
use Illuminate\Support\MessageBag;

class SiteController extends Controller {

	
	public function __construct(Route $route){
	        
	       	$this->middleware('auth');
			//$this->middleware('auth.check_logic');
	 }
	 
	public function snapshot_creation(){
		
		Log::info("snapshot_creation:");
		
		$label = Input::get('snapshot_label_form');
		$site_id = Input::get('site_id_form');
		
		//ERROR CASES FOR BACKUP MODEL
		//1. LABEL FORMAT 
		//2. SITE_ID AND LABEL UNIQUE
		
		if(!preg_match('/^[a-zA-Z0-9-_]+$/',$label)){
			return redirect('/sites')->withErrors(['Label' => 'Label format error. Please use only letters, numbers, -, or _ for characters']);
		}
		
		$check_backup = Backup::where("label",$label)->where("site_id",$site_id)->first();
		if(isset($check_backup)){
			return redirect('/sites')->withErrors(['Label' => 'This label is already used.']);
		}
		
		$site = Site::findOrFail(Input::get('site_id_form'));
		$site->snapshot_creation(Input::get('snapshot_label_form'));
		$site->save();
		
		return Redirect::to('/sites/snapshots');
	}
	
	public function restore_backup(){
		
		//3 CASES
		//1 RESTORE TO NEW DOMAIN RANDOM DOMAIN
		//2 RESTORE TO NEW DOMAIN WITH DOMAIN PARAM
		//3 RESTORE TO SITE CURRENT DOMAIN
		
		Log::info("backup_id_form: ".Input::get('backup_id_form'));
		Log::info("domain_form: ".Input::get('domain_form'));
		Log::info("backup_action_form: ".Input::get('backup_action_form'));
		
		$backup_id = Input::get('backup_id_form');
		$domain = Input::get('domain_form');
		$backup_action = Input::get('backup_action_form');
		
		$backup = Backup::findOrFail($backup_id);
		$pete_options = new PeteOption();
		$app_root = $pete_options->get_meta_value('app_root');
		$domain_template = $pete_options->get_meta_value('domain_template');
		$backup_file = "$app_root/Pete/backups/$backup->site_id/$backup->name-$backup->schedulling.tar.gz";
		
		//CASE 1
		if(($backup_action == "restore_to_new_domain") & ($domain=="")){
			
			$new_site = new Site();
			$new_site->theme = $backup->theme;
			$new_site->action_name = "Backup Restore";
			$new_site->name = $backup->generate_uniq_name();
			$new_site->url = $new_site->name.".".$domain_template;
			
		}else if(($backup_action == "restore_to_new_domain") & ($domain!="")){
			
			$check_site = Site::where("url",$domain)->first();
			if(isset($check_site)){
				return redirect('/sites')->withErrors(['URL' => 'This URL is already used.']);
			}
			
			$uniqid = uniqid();
			$uniqid = "restore_".$uniqid;
			$new_site = new Site();
			$new_site->theme = $backup->theme;
			$new_site->action_name = "Backup Restore";
			$new_site->name = $uniqid.$backup->name;
			$new_site->url = $domain;
			
		}else if($backup_action == "restore_to_current_domain"){
			
			$uniqid = uniqid();
			$uniqid = "restore_".$uniqid;
			$new_site = new Site();
			$new_site->theme = $backup->theme;
			$new_site->action_name = "Backup Restore";
			$new_site->name = $uniqid.$backup->name;
			$new_site->url = $backup->url;
			
			$site_to_replace = Site::where("url",$backup->url)->first();
			if(isset($site_to_replace)){
				$site_to_replace->delete_wordpress();
				$site_to_replace->delete();
			}
		}
		
		$new_site->import_wordpress($backup_file);
		return Redirect::to('/sites?success=true');
	}
	
	public function snapshots(){

		//$backups = Backup::orderBy('backups.id', 'desc')->where("manual",true)->get();
		$user = Auth::user();
		
		$backups = Backup::orderBy('backups.created_at', 'asc')
			    ->select(DB::raw('backups.id, backups.schedulling, backups.file_name,sites.name, sites.url'))
				->join('sites', 'sites.id', '=', 'backups.site_id')
				->where("sites.user_id",$user->id)->where("manual",true)->get();
		
		$viewsw = "sites";
		return view('sites.backups', compact('backups','viewsw'));
		
	}
	
	public function backups(){
		
		$user = Auth::user();
		
		$backups = Backup::orderBy('backups.created_at', 'asc')
			    ->select(DB::raw('backups.id, backups.schedulling, backups.file_name,sites.name, sites.url'))
				->join('sites', 'sites.id', '=', 'backups.site_id')
				->where("sites.user_id",$user->id)->where("manual",false)->get();
		
		$viewsw = "sites";
		return view('sites.backups', compact('backups','viewsw'));
		
	}
	
	public function delete_backup(){
		
		$backup = Backup::findOrFail(Input::get('backup_id'));
		$view = Input::get('view');
		$backup->delete();
		return Redirect::to("/sites/$view");
		
	}
	
	
	
	public function register_your_license(){
		
		$viewsw = "sites";
		$validation_url = Option::get_meta_value('validation_url');
		return view('oregisters.register_your_license', compact('validation_url','viewsw'));
		
	}
	
	
	public function export(){
		
		$viewsw = "sites";
		$site = Site::findOrFail(Input::get('id'));
		$site->export_wordpress();
		$exporturl = $site->file_to_download;
		$sites = Site::orderBy('id', 'desc')->paginate(10);
		return view('sites.index', compact('sites','exporturl','viewsw'));
	  
	}
	
	public function reload_server(){
		
		Site::reload_server();
		return response()->json(["OK" => "OK"]);  
	}
	
	public function trash(){
		
		$user = Auth::user();
		$viewsw = "trash";
		$sites = $user->my_trash_sites()->paginate(10);
		return view('sites.trash', compact('sites','viewsw'));
		
	}
	
	public function suspend(){
		
		$site = Site::findOrFail(Input::get('id'));
		$site->suspend = true;
		$site->suspend_wordpress();
		$site->save();
		return Redirect::to('/?success=true&site_id='.$site->id);
	  
	}
	
	public function site_continue(){
		
		$site = Site::findOrFail(Input::get('id'));
		$site->suspend = false;
		$site->continue_wordpress();
		$site->save();
		return Redirect::to('/?success=true&site_id='.$site->id);
	  
	}
	
	public function get_sites(){
		
		$app_name = Input::get('app_name');
		if($app_name == "Wordpress"){
		  $result = Site::where('app_name', "Wordpress")->orderBy('id', 'desc')->get();
		}else if($app_name == "Wordpress+laravel"){
		  $result = Site::where('app_name', "Wordpress")->orderBy('id', 'desc')->get();
		  for ($i = 0; $i < count($result); $i++) {
			if(substr_count($result[0]->domain, '.') > 2) {
				unset($array[$i]);
			}
			  
		  }
		}
	    return response()->json($result);
	  
	}
	
	
	public function restore(){
		$site = Site::withTrashed()->findOrFail(Input::get('id'));
		$site->restore();
		$site->restore_wordpress();
		return Redirect::to('/?success=true&site_id='.$site->id);
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Auth::user();
		$viewsw = "/sites";
		
		if($user->admin){
			$sites = Site::orderBy('id', 'desc')->where("app_name","WordPress")->paginate(50);
		}else{
			$sites = $user->my_sites()->where("app_name","WordPress")->paginate(10);
		}
		
		$success = Input::get('success');
		$site_id = Input::get('site_id');
		return view('sites.index', compact('sites','success','site_id','viewsw'));
	}
	
	public function search()
	{
		$viewsw = "sites";
	    $sites = Site::orderBy('id', 'desc')->where('url', 'like', '%' . Input::get('data') . '%')->paginate(50);
		$data = Input::get('data');
		return view('sites.index', compact('sites','current_user','first_time','viewsw','data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{   
		$viewsw = "/sites";
		return view('sites.create',compact('sites','viewsw'));
	}
	
	public function wordpress_plus_laravel(){
		
		$num = substr(PHP_VERSION, 0, 3);
		$float_version = (float)$num;
		
		if($float_version < 7.1){
        	return redirect('sites/create')->withErrors("The PHP version must be >= 7.1 to activate WordPress Plus Laravel functionality.");
		}
		
		$viewsw = "wordpress+laravel";
		return view('sites.wordpress_plus_laravel',compact('viewsw'));
	}
	
	public function laravel(){
		
		$num = substr(PHP_VERSION, 0, 3);
		$float_version = (float)$num;
		
		if($float_version < 7.1){
        	return redirect('sites/create')->withErrors("The PHP version must be >= 7.1 to activate WordPress Plus Laravel functionality.");
		}
		
		$viewsw = "laravel";
		return view('sites.laravel',compact('viewsw'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$pete_options = new PeteOption();
		$user = Auth::user();
		$fields_to_validator = $request->all();
		
		$site = new Site();
		$site->output = "";
		$site->user_id = $user->id;
		$site->app_name = $request->input("app_name");
		$site->action_name = $request->input("action_name");
		$site->to_clone_project_id = $request->input("to_clone_project_id");
		$site->name = $request->input("name");
		$site->to_import_project = $request->input("to_import_project");
		$site->user_id = $user->id;
		$site->url = $request->input("url");
		$site->big_file_route = $request->input("big_file_route");
		$site->laravel_version = $request->input("selected_version");	
		
		if(($user->shared) & ($user->has_one_site())){
			return Redirect::back()->withErrors(['Please upgrade your plan.']);
		}
		
		$app_root = $pete_options->get_meta_value('app_root');
		if($pete_options->get_meta_value('domain_template')){
	
			$site->url = $site->url . "." . $pete_options->get_meta_value('domain_template');
		}
		
		$validator = Validator::make($fields_to_validator, [
			'name' =>  array('required', 'regex:/^[a-zA-Z0-9-_]+$/','unique:sites'),
			'url' => 'required|unique:sites',
		]);
		
     	if ($validator->fails()) {
			
	        return redirect('sites/create')
	        		->withErrors($validator)
	        			->withInput();
     	 }
		
		if($request->file('filem')!= ""){
			
			$file = $request->file('filem');
	        // SET UPLOAD PATH
	        $destinationPath = 'uploads';
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
	        $fileName = rand(11111, 99999) . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$site->zip_file_url = $fileName;
		
		}
		
		if($site->action_name == "New"){
			$site->new_wordpress();
		}else if($site->action_name == "Clone"){
			$site->clone_wordpress();
		}else if($site->action_name == "Import"){
			#$site->import_wordpress("https://www.dropbox.com/s/fhm4i0sq895wp68/amaretto.tar.gz");
			$site->import_wordpress();
		}
			
		return Redirect::to('/sites/'.$site->id .'/edit' .'?success=' . 'true');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$viewsw = "/sites";
		$site = Site::findOrFail($id);
		$adomains = Adomain::orderBy('id', 'desc')->where('site_id', $site->id)->get();	
		$success = Input::get('success');
		return view('sites.edit', compact('site','adomains','success','viewsw'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = Auth::user();
		$site = Site::findOrFail($id);
		$site->delete_wordpress();
		$site->delete();
		$debug = env('DEBUG');
		if($debug == "active"){
			Log::info('Ouput deleteDebug' . $site->output);
		}
		
		if($user->id == $site->user_id){
			$site->delete();
		}
		
		return Redirect::to('/?success=true');
	}
	
    public function force_delete(){
	   
	    $user = Auth::user();
 		$site = Site::onlyTrashed()->findOrFail(Input::get('site_id'));	
 		$site->force_delete_wordpress();
 		
		if($user->admin){
			$site->forceDelete();
		}else if($user->id == $site->user_id){
			$site->forceDelete();
		}
		
 		return Redirect::to('sites/trash');
	
    }
	
	
	
	

}
