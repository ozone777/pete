<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Option;
use Illuminate\Http\Request;

class OptionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct()
	    {
	        $this->middleware('auth');
			//$this->middleware('auth.check_logic');
	    }
	
	public function index()
	{
		$viewsw = "options";
		$options = Option::where("visible",true)->orderBy('id', 'desc')->paginate(25);

		return view('options.index', compact('options','viewsw'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$viewsw = "options";
		return view('options.create',compact('viewsw'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		$option = new Option();

		$option->option_name = $request->input("option_name");
        $option->option_value = $request->input("option_value");
		$option->visible = true;
		$option->save();

		return redirect()->route('options.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$viewsw = "options";
		$option = Option::findOrFail($id);

		return view('options.show', compact('option','viewsw'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$viewsw = "options";
		$option = Option::findOrFail($id);

		return view('options.edit', compact('option','viewsw'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$option = Option::findOrFail($id);

		$option->option_name = $request->input("option_name");
        $option->option_value = $request->input("option_value");

		$option->save();

		return redirect()->route('options.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$option = Option::findOrFail($id);
		$option->delete();

		return redirect()->route('options.index')->with('message', 'Item deleted successfully.');
	}

}
