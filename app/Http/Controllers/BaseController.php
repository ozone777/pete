<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Log;
use App\PeteOption;

class BaseController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct()
	    {
	       $this->middleware('auth');
			//$this->middleware('auth.check_logic');
	    }
	
	public function sites_list(){
		
		$sites = Site::orderBy('id', 'desc')->get();
		echo Input::get('callback') . '('.json_encode($sites).')';
			
	}
	
	public function pete_plugins(){
		
		$viewsw = "/pete_plugins";
		$dashboard_url = env("DASHBOARD_URL");
		
		return view('base.pete_plugins',compact('viewsw','dashboard_url'));
	 
	}
	
	public function pete_plugins_install(){
		
		$pete_options = new PeteOption();
		$app_root = $pete_options->get_meta_value('app_root');
		$os = $pete_options->get_meta_value('os');
		$script = Input::get('install_script');
		
		chdir("$app_root/Pete");
		$output = "";
		
	   	if($os == "mac"){
			
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer.phar");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
			
	   	}else if(($os == "linux") || ($os == "olinux")){
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}
		
		//Remove break line characters
		$script = str_replace("\r", "", $script);
		$script = str_replace("\n", "", $script);
		$script = str_replace("<CR>", "", $script);
		
		//Convert script in array
		$commands_array = explode(";", $script);
		
		foreach ($commands_array as $command) {
			Log::info("command: ".$command);
			$output .= shell_exec($command);
		}
		
		Log::info($output);
		
		return Redirect::back();
	}
	
	public function pete_plugins_uninstall(){
		
		$pete_options = new PeteOption();
		$app_root = $pete_options->get_meta_value('app_root');
		$os = $pete_options->get_meta_value('os');
		$script = Input::get('uninstall_script');
		
		chdir("$app_root/Pete");
		$output = "";
		
	   	if($os == "mac"){
			
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer.phar");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
			
	   	}else if(($os == "linux") || ($os == "olinux")){
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}
		
		//Remove break line characters
		$script = str_replace("\r", "", $script);
		$script = str_replace("\n", "", $script);
		$script = str_replace("<CR>", "", $script);
		
		//Convert script in array
		$commands_array = explode(";", $script);
		
		foreach ($commands_array as $command) {
			Log::info("command: ".$command);
			$output .= shell_exec($command);
		}
		
		Log::info($output);
		
		return Redirect::back();
	}
	
	public function pete_plugins_update(){
		$pete_options = new PeteOption();
		$app_root = $pete_options->get_meta_value('app_root');
		
		$script = Input::get('update_script');
		$pointer = "cd $app_root/Pete";
		$output = shell_exec($pointer ." ". $script);
		
		Log::info("pete_plugins_update method");
		Log::info($script);
		Log::info($output);
		
		return Redirect::back();
	}

}
