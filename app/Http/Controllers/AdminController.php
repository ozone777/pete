<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Site;
use App\User;
use Illuminate\Http\Request;
use Input;
use Log;
use Illuminate\Support\Facades\Redirect;


class AdminController extends Controller {

	public function __construct()
	    {
	        #$this->middleware('csrf');
	        $this->middleware('auth');
			
	    }
	
	public function list_users(){
		
		$users = User::orderBy('id', 'desc')->paginate(10);
		$viewsw = "/list_users";		
	    return view('admin.list_users',compact('users','viewsw'));
		
	}
	
	 public function edit_user()
	{
		$viewsw = "/list_users";		
		$user = User::findOrFail(Input::get('id'));
		return view('admin.edit_user', compact('user','viewsw'));
	}
	
	 public function delete_user()
	 {
		 $user = User::findOrFail(Input::get('user_id'));
		 $user->delete();
		 return Redirect::back();
	}
	
	

}
