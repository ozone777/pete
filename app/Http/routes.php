<?php
use App\Option;
use App\Site;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('sites/testing', 'SiteController@testing');

Route::get('sites/get_db_info', 'SiteController@get_db_info');

Route::get('sites/get_cms_info', 'SiteController@get_cms_info');

Route::get('sites/export','SiteController@export');

Route::get('sites/snapshots','SiteController@snapshots');

Route::get('sites/backups','SiteController@backups');

Route::post('snapshot_creation','SiteController@snapshot_creation');

Route::post('restore_backup','SiteController@restore_backup');

Route::post('delete_backup','SiteController@delete_backup');

Route::get('sites/addkeys','SiteController@addkeys');

Route::get('sites/add_alias','SiteController@add_alias');

Route::get('sites/get_sites','SiteController@get_sites');

Route::get('sites/trash','SiteController@trash');

Route::get('sites/suspend','SiteController@suspend');

Route::get('sites/site_continue','SiteController@site_continue');

Route::get('sites/restore','SiteController@restore');

Route::get('sites/search','SiteController@search');

Route::get('sites/wordpress_plus_laravel','SiteController@wordpress_plus_laravel');

Route::get('sites/laravel','SiteController@laravel');

Route::post('force_delete', 'SiteController@force_delete');

Route::get('update_license','OregisterController@update_license');

Route::get("reload_server",'SiteController@reload_server');

Route::get('register_your_license','OregisterController@register_your_license');

Route::resource("sites","SiteController");

Route::resource("options","OptionController");

Route::resource("adomains","AdomainController");


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//API ACTIONS ROUTES
Route::get('/list_backups', 'APIController@list_backups');
Route::get('/delete_backup', 'APIController@delete_backup');
Route::get('/list_sites', 'APIController@list_sites');
Route::get('/list_trash', 'APIController@list_trash');

//Admin Routes
Route::get('/list_users', 'AdminController@list_users');
Route::get('/edit_user', 'AdminController@edit_user');
Route::post('/delete_user', 'AdminController@delete_user');

Route::get('/pete_plugins','BaseController@pete_plugins');
Route::post('/pete_plugins_install','BaseController@pete_plugins_install');
Route::post('/pete_plugins_uninstall','BaseController@pete_plugins_uninstall');
Route::post('/pete_plugins_update','BaseController@pete_plugins_update');

Route::get('/get_domains', function () {
	
	$sites = Site::orderBy('id', 'desc')->get();
    return response()->json($sites);
});


Route::get('/', array('as' => 'sites.index', 'uses' => 'SiteController@index'));
Route::get('/home', array('as' => 'sites.index', 'uses' => 'SiteController@index'));


