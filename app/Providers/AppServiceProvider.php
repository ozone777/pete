<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use View;
use Route;
use App\Option;
use App\PeteOption;
use App\Backup;
use Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
     	   
   	View::composer('*', function($view){
		
	  	$current_user = Auth::user();
	  	$pete_options = new PeteOption();
		$sidebar_options = Option::where("category","sidebar")->orderBy("order","asc")->get();
		
	  	$version = Option::get_meta_value('version');
	 	// $controller = substr(class_basename(Route::currentRouteAction()), 0, (strpos(class_basename(Route::currentRouteAction()), '@') -0) );
	 	// $action = explode('@',Route::currentRouteAction())[1];
		
		 if(($pete_options->get_meta_value('os') == "mac" ) || ($pete_options->get_meta_value('os') == "darwin" )){
			
			$apache_v_dialog=explode(' P',apache_get_version())[0];
			$php_v_dialog = "PHP: ".PHP_VERSION;
			$mysql_v_dialog = shell_exec('mysql -V');
			
		}else if($pete_options->get_meta_value('os') == "olinux" ){
			///
			$apache_v_dialog = shell_exec('apache2 -v | grep version');
			$php_v_dialog = shell_exec('php -r "echo phpversion();"');
			$mysql_v_dialog = shell_exec('mysql -V');
			
		}else {
			$apache_v_dialog=explode(' P',apache_get_version())[0];
			$php_v_dialog = PHP_VERSION;
			$mysql_v_dialog=explode(', s',shell_exec('mysql -V'))[0];
		}
			
   	  	$view->with(compact('current_user','pete_options','apache_v_dialog','php_v_dialog','mysql_v_dialog','sidebar_options'));
	  
    });
	
	Backup::deleting(function($model){
	  $pete_options = new PeteOption();
	  $app_root = Option::get_meta_value('app_root');
	  
	  $debug = env('DEBUG');
	  //$command = "rm -rf $app_root"."/"."Pete/public/backups"."/$model->file_name";
	  //$result = shell_exec("rm -rf $app_root"."/"."Pete/backups"."/$model->id."/"."$model->file_name); 
	  
	  Log::info("rm -rf $app_root/Pete/backups/$model->site_id/$model->file_name");
	  
	  $result = shell_exec("rm -rf $app_root/Pete/backups/$model->site_id/$model->file_name"); 
	  
  	  if($debug == "active"){
  		//Log::info('Pete: ' . $command);
		Log::info('Delete backup Output: ' . $result);
  	  }	  
	  
	});
	
		
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
