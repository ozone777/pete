<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;
use App\Secure;
use DB;
use App\Pete;
use App\Backup;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\PeteOption;
use Crypt;

class Site extends Model {
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
	
	public function user()
	{
	    return $this->belongsTo('App\User');
	}
		
	public static function reload_server() {
		
		Log::info("entro en reload_server");
		$output = "";
		$pete_options = new PeteOption();	
		$os = $pete_options->get_meta_value('os');
		
		$app_root = $pete_options->get_meta_value('app_root');
		chdir("$app_root/Pete/scripts/");
		Log::info("os: $os");
		
   	   	if ($os == "mac"){
			Log::info("entro en reload_server mac");
		 	$output = shell_exec('sudo /usr/local/opt/httpd/bin/httpd -k graceful'); 
			Log::info($output);
	   	}else if($os =="olinux"){
			//$output = shell_exec("cd /etc/apache2/sites-enabled && ln -s /etc/apache2/sites-available/$this->name.conf $this->name.conf"); 
			$output = shell_exec("sudo /etc/init.d/apache2 reload");
	   	}else {
			switch (PHP_VERSION) {
				case "5.6.38":
					$output=shell_exec('restart.cmd -n Aphp5_6'); 
				break;
				case "7.0.32":
					$output=shell_exec('restart.cmd -n Aphp7_0'); 
				break;
				case "7.1.24":
					$output=shell_exec('restart.cmd -n Aphp7_1'); 
				break;
				case "7.2.12":
					$output=shell_exec('restart.cmd -n Aphp7_2'); 
				break;
			}
		}
		Log::info("Reload server: $output");
		return $output;
	}
	
	public function normalize_name(){
		if (strpos($this->name, '_') !== false) {
			$aux = $this->name;
			$pos = strpos($aux, "_");
			$str1 = substr($aux,0,$pos+1);
			$this->name = str_replace($str1,"",$aux);
		}
	}
	
	public function snapshot_creation($label){
		
  	  $pete_options = new PeteOption();
  	  $app_root = $pete_options->get_meta_value('app_root');
      $mysql_bin = $pete_options->get_meta_value('mysql_bin');
  	  $server_conf = $pete_options->get_meta_value('server_conf');
  	  $os = $pete_options->get_meta_value('os');
  	  $server = $pete_options->get_meta_value('server');
  	  $server_version = $pete_options->get_meta_value('server_version');
  	  $this->wp_load_path = $app_root . "/" . $this->name;
	  //$this->snapshot_date = date("Y-m-d H:i:s");
	  
  	  $mysqldump = $mysql_bin . "mysqldump";
  	  $id = $this->id;
	  
  	  $db_root_pass = env('ROOT_PASS');
  	  $debug = env('DEBUG');

  	  chdir("$app_root/Pete/scripts/");
  	  
 	  require("$this->wp_load_path/wp-config.php");	  
  	  $db_name = constant('DB_NAME');
	  
	  //SET BACKUP
	  $backup = new Backup();
	  $backup->site_id = $this->id;
	  
	  $backup->theme = $this->theme;
	  $backup->first_password = $this->first_password;
	  $backup->wp_user = $this->wp_user;
	  
	  $backup->name = $this->name;
	  $backup->url =  $this->url;
	  $backup->schedulling = $label;
	  $backup->file_name = $this->name."-".$backup->schedulling.".tar.gz";
	  $backup->manual = true;
		  
  	  $command = "./snapshot_creation.sh -n {$this->name} -p {$db_root_pass} -r {$app_root} -m {$mysqldump} -v {$os} -w {$server} -t {$server_version} -u {$this->url} -a {$this->app_name} -q {$db_name} -i {$table_prefix} -d {$this->id} -s {$label} -k {$debug}";
  	  
	  $output = shell_exec($command);
	  
  	  if($debug == "active"){
  		Log::info('Snapshot creation command: ' . $command);
  		Log::info("Output");
		Log::info($output);
  	  }

	  $backup->save();
  	  $this->save();	  
  	}
		
    public static function domain_check($domain) {
	  	$site = Site::where('url', $domain)->first();
		if($site){
			return true;
		}else{
			return false;
		}
	}
	
    public static function name_check($name) {
	  	$site = Site::where('name', $name)->first();
		if($site){
			return true;
		}else{
			return false;
		}
	}
	
	public function force_delete_wordpress() {
			
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$os_version = $pete_options->get_meta_value('os_version');
		
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$project_name = str_replace("_odeleted_$this->id","",$this->name);
			
		$debug = env('DEBUG');
		chdir("$app_root/Pete/scripts/");
		
		if ($os=="Windows_NT") {
			$command = "{$os}_force_delete.cmd -n {$project_name} -r {$app_root} -a {$server_conf} -v {$os} -j {$os_version} -s {$this->id} -l {$server} -w {$server_version} -k {$debug}";
		}else {
			$command = "./{$os}_force_delete.sh -n {$project_name} -r {$app_root} -a {$server_conf} -v {$os} -j {$os_version} -s {$this->id} -l {$server} -w {$server_version} -k {$debug}";
		}
		
  	  	if($debug == "active"){
  			Log::info('Pete force_delete: ' . $command);
			$this->output = $this->output . "#######FORCE DELETE#######\n";
  	  	}
		
		$this->output = $this->output . shell_exec($command);
		Log::info('Output force_delete: ' . $this->output);
		$this->save();
	}
		
	public function suspend_wordpress() {
		
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$app_root = $pete_options->get_meta_value('app_root');	
			
		$project_name = $this->name;
		$app_name = $this->app_name ;
		$id = $this->id;
		$project_url = $this->url;
		
		$debug = env('DEBUG');
		$db_root_pass = env('ROOT_PASS');
		
		chdir("$app_root/Pete/scripts/");

		if ($os=="Windows_NT") {
			$command = "{$os}_suspend.cmd -n {$project_name} -r {$app_root} -a {$server_conf} -v {$server} -w {$server_version } -u {$project_url} -o {$os_version} -j {$os_version} -k {$debug}";
		}else {
			$command = "./{$os}_suspend.sh -n {$project_name} -r {$app_root} -a {$server_conf} -v {$server} -w {$server_version } -u {$project_url} -o {$os_version} -j {$os_version} -k {$debug}";
		}

  	  	if($debug == "active"){
  			Log::info('Pete Suspend: ' . $command);
			$this->output = $this->output . "#######SUSPEND#######\n";
  	  	}
		
		$this->output = $this->output . shell_exec($command);
		$this->suspend = true;
		$this->save();
	}
	
	public function continue_wordpress() {
		
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
	    $app_root = $pete_options->get_meta_value('app_root');
	    $server_conf = $pete_options->get_meta_value('server_conf');
				
		$app_name = $this->app_name ;
		$id = $this->id;
		$project_url = $this->url;	
        $project_name = $this->name;
		
		$debug = env('DEBUG');
		
		chdir("$app_root/Pete/scripts/");

		if ($os=="Windows_NT") {
			$command = "{$os}_continue.cmd -n {$project_name} -r {$app_root} -a {$server_conf} -v {$server} -w {$server_version} -u {$project_url} -o {$os_version} -j {$os_version} -k {$debug}";
		}else {
			$command = "./{$os}_continue.sh -n {$project_name} -r {$app_root} -a {$server_conf} -v {$server} -w {$server_version} -u {$project_url} -o {$os_version} -j {$os_version} -k {$debug}";
		}

  	  	if($debug == "active"){
  			Log::info('Pete Continue: ' . $command);
			$this->output = $this->output . "#######CONTINUE#######\n";
  	  	}
		
		$this->output = $this->output . shell_exec($command);
		$this->suspend = false;
		$this->save();
	}
	
	public function delete_wordpress() {
			
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$os_version = $pete_options->get_meta_value('os_version');
		
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$id = $this->id;
		$project_name = $this->name;
			
		$debug = env('DEBUG');
		
		chdir("$app_root/Pete/scripts/");
			
		if ($os=="Windows_NT") {
			$command = "{$os}_delete.cmd -n {$project_name} -r {$app_root} -a {$server_conf} -v {$os} -s {$id} -j {$os_version} -l {$server} -w {$server_version} -k {$debug} -url {$this->url}";
		}else {
			$command = "./{$os}_delete.sh -n {$project_name} -r {$app_root} -a {$server_conf} -v {$os} -s {$id} -j {$os_version} -l {$server} -w {$server_version} -k {$debug}";
		}

  	  	if($debug == "active"){
  			Log::info('Pete Delete: ' . $command);
			$this->output = $this->output . "#######DELETE#######\n";
  	  	}
		
		$this->output = $this->output . shell_exec($command);
		
		$this->url = $this->url . "_odeleted_" . $this->id;
		$this->name = $this->name . "_odeleted_" . $this->id;
		
		$this->save();
	}
	
	public function restore_wordpress() {
		
		$this->url = str_replace("_odeleted_$this->id","",$this->url);
		$this->name = str_replace("_odeleted_$this->id","",$this->name);
		
		$site_url = $this->url;	
        $site_name = $this->name;
		
		$pete_options = new PeteOption();
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os_version = $pete_options->get_meta_value('os_version');
  	    $os = $pete_options->get_meta_value('os');
		
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$id = $this->id;
		$db_root_pass = env('ROOT_PASS');
		$debug = env('DEBUG');
		
		chdir("$app_root/Pete/scripts/");
	
		if ($os=="Windows_NT") {
			$command = "{$os}_restore.cmd -n {$site_name} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -v {$server} -w {$server_version} -s {$this->id} -u {$this->url} -j {$os_version} -k {$debug}";
		}else {
			$command = "./{$os}_restore.sh -n {$site_name} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -v {$server} -w {$server_version} -s {$this->id} -u {$this->url} -j {$os_version} -k {$debug}";
		}
		
  	    if($debug == "active"){
  		  Log::info('Pete Restore: ' . $command);
		  $this->output = $this->output . "#######RESTORE#######\n";
  	    }
		
		$this->output = $this->output . shell_exec($command);
		$this->save();
		
	}
	
	public function export_wordpress() {

	  $pete_options = new PeteOption();
	  $app_root = $pete_options->get_meta_value('app_root');
      $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	  $server_conf = $pete_options->get_meta_value('server_conf');
	  $os = $pete_options->get_meta_value('os');
	  $server = $pete_options->get_meta_value('server');
	  $server_version = $pete_options->get_meta_value('server_version');
	  $this->wp_load_path = $app_root . "/" . $this->name;
	  
	  $mysqldump = $mysql_bin . "mysqldump";
	  $id = $this->id;
	  
	  $db_root_pass = env('ROOT_PASS');
	  $debug = env('DEBUG');

	  chdir("$app_root/Pete/scripts/");
	  if ($os=="Windows_NT") {
		  
		  $this->wp_load_path = $app_root . "\\" . $this->name;
		  require("$this->wp_load_path\wp-config.php");
		  $this->DB_NAME = constant('DB_NAME');
		  
		  $command = "{$os}_export.cmd -n {$this->name} -p {$db_root_pass} -r {$app_root} -m {$mysqldump} -v {$os} -w {$server} -t {$server_version} -u {$this->url} -a {$this->app_name} -k {$debug} -odb {$this->DB_NAME} -pr {$table_prefix}";
	  }else {
		  
		  require("$this->wp_load_path/wp-config.php");
		  
		  $db_name = constant('DB_NAME');
		  
		  $command = "./{$os}_export.sh -n {$this->name} -p {$db_root_pass} -r {$app_root} -m {$mysqldump} -v {$os} -w {$server} -t {$server_version} -u {$this->url} -a {$this->app_name} -q {$db_name} -i {$table_prefix} -k {$debug}";
	  }
	  
	  if($debug == "active"){
		Log::info('Pete Export: ' . $command);
		$this->output = $this->output . "#######EXPORT#######\n";
	  }
	  
	  $this->output = $this->output . shell_exec($command);
	  
	  if(($os == "mac") || ($os == "linux") || ($os == "olinux")){
		$this->file_to_download = "export/{$this->name}.tar.gz";
	  }else{
	  	$this->file_to_download = "export/{$this->name}.zip";
	  }
	  
	  $this->save();
	}
	
	public function new_wordpress() {
		
		$pete_options = new PeteOption();
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
			
	    $db_name = "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$user = User::find($this->user_id);
		if($user->shared){
			$db_user =  Crypt::decrypt($user->user_db);
			$db_user_pass = Crypt::decrypt($user->user_db_pass);
		}else{
		    $db_user = "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		    $db_user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		} 
	
	    $wpkey1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey3 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey4 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey5 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey6 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey7 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey8 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
		
		$db_root_pass = env('ROOT_PASS');
		$mysqlcommand = $mysql_bin . "mysql";
		$debug = env('DEBUG');
		
		chdir("$app_root/Pete/scripts/");
		
		if ($os=="Windows_NT") {
			$command = "{$os}_new.cmd -n {$this->name} -u {$this->url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -x {$db_name} -y {$db_user} -z {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -j {$os_version} -t {$server} -w {$server_version} -k {$debug} ";
		}else {
			$command = "./{$os}_new.sh -n {$this->name} -u {$this->url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -x {$db_name} -y {$db_user} -z {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -j {$os_version} -t {$server} -w {$server_version} -k {$debug} ";
		}

 	   if($debug == "active"){
 	     Log::info('Pete ' .$this->action_name . ": " . $command);
 		 $this->output = $this->output . "#######".$this->action_name."#######\n";	 
 	   }
	   
	   $this->app_name = "WordPress";
	   $this->output = $this->output . shell_exec($command);
	   $this->save();
		
	}
	
	public function clone_wordpress() {
		
		$pete_options = new PeteOption();
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$apache_version = $pete_options->get_meta_value('apache_version');
		
		
	    $db_name = "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$user = User::find($this->user_id);
		if($user->shared){
			$db_user =  Crypt::decrypt($user->user_db);
			$db_user_pass = Crypt::decrypt($user->user_db_pass);
		}else{
		    $db_user = "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		    $db_user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		} 
	
	    $wpkey1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey3 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey4 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey5 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey6 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey7 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey8 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
		
		$db_root_pass = env('ROOT_PASS');
		$mysqlcommand = $mysql_bin . "mysql";
		$debug = env('DEBUG');
		$mysqldump = $mysql_bin . "mysqldump";
		
		$to_clone_project = Site::findOrFail($this->to_clone_project_id);
		$this->wp_load_path = $app_root . "/" . $to_clone_project->name;
		
		chdir("$app_root/Pete/scripts/");

		if ($os=="Windows_NT") {
			
			$this->wp_load_path = $app_root . "\\" . $to_clone_project->name;
			require("$this->wp_load_path\wp-config.php");
			$this->DB_NAME = constant('DB_NAME'); 
			
			$command = "{$os}_clone.cmd -n {$this->name} -o {$to_clone_project->name} -u {$this->url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -q {$mysqldump} -a {$server_conf}  -x {$db_name} -y {$db_user} -z {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -l {$to_clone_project->url} -j {$os_version} -t {$server} -w {$server_version} -k {$debug} -odb {$this->DB_NAME} -pr {$table_prefix}";
		}else {
			
  		  require("$this->wp_load_path/wp-config.php");
  		  $odb = constant('DB_NAME');
		  
			$command = "./{$os}_clone.sh -n {$this->name} -o {$to_clone_project->name} -u {$this->url} -p {$db_root_pass} -r {$app_root} -q {$mysqldump} -a {$server_conf} -x {$db_name} -y {$db_user} -z {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -l {$to_clone_project->url} -j {$os_version} -t {$server} -w {$server_version} -s {$odb} -m {$table_prefix} -k {$debug}";
		}	
		
 	   if($debug == "active"){
 	     Log::info('Pete ' .$this->action_name . ": " . $command);
 		 $this->output = $this->output . "#######".$this->action_name."#######\n";	 
 	   }
	   
	   $this->app_name = "WordPress";
	   $this->output = $this->output . shell_exec($command);
	   $this->save();
		
	}
	
	public function import_wordpress($template_url="none",$database_variables=null) {
		
		$pete_options = new PeteOption();
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$apache_version = $pete_options->get_meta_value('apache_version');
		
		if(isset($database_variables)){
			$db_name = $database_variables["db_name"];
			$db_user = $database_variables["db_user"];
			$db_user_pass = $database_variables["db_user_pass"];
			/*
			Log::info("entro en database_variables");
			Log::info("db_name: ".$db_name);
			Log::info("db_user: ".$db_user);
			Log::info("db_user_pass: ".$db_user_pass);
			*/
		}else{
		    $db_name = "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		    $db_user = "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		    $db_user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		}
	
	    $wpkey1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey3 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey4 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey5 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey6 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey7 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey8 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
		
		$db_root_pass = env('ROOT_PASS');
		$mysqlcommand = $mysql_bin . "mysql";
		$debug = env('DEBUG');
		
		chdir("$app_root/Pete/scripts/");
		
		//TEMPLATE CASES
		if($template_url =="none"){
			if($this->big_file_route != ""){
				$file_route = $this->big_file_route;
			}else{
				$file_route = "$app_root/Pete/public/uploads/$this->zip_file_url";
			}
			$type="none";
		}else if(strpos($template_url, 'http') !== false) {
		   $type = "url";
		   $file_route = $template_url;
		}else{
		   $type = "file";
		   $file_route = $template_url;
		} 

		if ($os=="Windows_NT") {
			$command = "{$os}_import.cmd -n {$this->name} -u {$this->url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -x {$db_name} -y {$db_user} -l {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -j {$os_version} -t {$server} -w {$server_version} -o {$file_route} -s {$type} -k {$debug}" ;
		}else {
			$command = "./{$os}_import.sh -n {$this->name} -u {$this->url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -x {$db_name} -y {$db_user} -l {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -j {$os_version} -t {$server} -w {$server_version} -o {$file_route} -s {$type} -k {$debug}" ;
		}	
		
 	   if($debug == "active"){
 	     Log::info('Pete ' .$this->action_name . ": " . $command);
 		 $this->output = $this->output . "#######".$this->action_name."#######\n";	 
 	   }
	   
	   $this->app_name = "WordPress";
	   $this->output = $this->output . shell_exec($command);
	   $this->save();
	   
	   Log::info($this->output);
		
	}	
	
	public function wordpress_laravel() {
		
		
		$pete_options = new PeteOption();
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$apache_version = $pete_options->get_meta_value('apache_version');
		
		$db_root_pass = env('ROOT_PASS');
		$mysqlcommand = $mysql_bin . "mysql";
		$debug = env('DEBUG');


		chdir("$app_root/Pete/scripts/");
		
		$target_site = Site::findOrFail($this->wordpress_laravel_target_id);
		$this->wordpress_laravel_url = $this->wordpress_laravel_name . '.' . $target_site->url;
		$this->url = $this->wordpress_laravel_url;	
	    $this->app_name = "WordPressPlusLaravel";
		$this->wp_load_path = $app_root . "/" . $target_site->name;
		$this->wp_url = $target_site->url;
		
		if($this->action_name == "new_wordpress_laravel"){
			$this->action_name = "New";
		}else{
			$this->action_name = "Import";
		}
		
		if((!$this->wordpress_laravel_git) || ($this->wordpress_laravel_git == "")){
	  		$this->wordpress_laravel_git_branch = "master";
	  		$this->wordpress_laravel_git = "https://github.com/peterconsuegra/wordpresspluslaravel.git";
		}
	  
		require("$this->wp_load_path/wp-config.php");
		$this->DB_USER = constant('DB_USER');
		$this->DB_NAME = constant('DB_NAME');
		$this->DB_PASSWORD = constant('DB_PASSWORD');

		if ($os=="Windows_NT") {
			$command = "{$os}_wordpress_laravel.cmd -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -b {$this->wordpress_laravel_git_branch} -g {$this->wordpress_laravel_git} -n {$this->name} -u {$this->wordpress_laravel_url} -j {$os_version} -v {$os} -l {$this->wp_load_path} -e {$this->wp_url} -t {$server} -w {$server_version} -c {$this->action_name} -o {$this->laravel_version} -k {$debug} -odb {$this->DB_NAME} -udb {$this->DB_USER} -pdb {$this->DB_PASSWORD}";
	  	}else {
			#hack project_name for multiple dashboard.* logic
			$this->name = $this->name . str_replace(".","",$this->wp_url);
			
			$command = "./{$os}_wordpress_laravel.sh -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -b {$this->wordpress_laravel_git_branch} -g {$this->wordpress_laravel_git} -n {$this->name} -u {$this->wordpress_laravel_url} -j {$os_version} -v {$os} -l {$this->wp_load_path} -e {$this->wp_url} -t {$server} -w {$server_version} -c {$this->action_name} -o {$this->laravel_version} -k {$debug} -q {$this->DB_NAME} -y {$this->DB_USER} -i {$this->DB_PASSWORD}";
		}	
	   
	   if($debug == "active"){
	     Log::info('Pete ' .$this->action_name . ": " . $command);
		 $this->output = $this->output . "#######".$this->action_name."#######\n";	 
	   }	
	
		putenv("DB_DATABASE=".DB_NAME);
		putenv("DB_USERNAME=".DB_USER);
		putenv("DB_PASSWORD=".DB_PASSWORD);
		
	   	if($os == "mac"){
			
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer.phar");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
			
	   	}else if(($os == "linux") || ($os == "olinux")){
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}
		
	   	$this->output = shell_exec($command);
		
	   	$this->save();
	  
	}
	
	public function action_laravel() {
		
		Log::info("entro en action_laravel");
		
		$pete_options = new PeteOption();
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$apache_version = $pete_options->get_meta_value('apache_version');
		
		$db_root_pass = env('ROOT_PASS');
		$mysqlcommand = $mysql_bin . "mysql";
		$debug = env('DEBUG');


		chdir("$app_root/Pete/scripts/");
		
		if($this->action_name == "new_laravel"){
			$this->action_name = "New";
			$this->wordpress_laravel_git_branch = "master";
			$this->wordpress_laravel_git = "github.com";
		}else{
			$this->laravel_version ="5";
			$this->action_name = "Import";
		}
		
		$this->app_name = "Laravel";
		
	    $db_name = "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
	    $db_user = "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
	    $db_user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
	
		$command = "./{$os}_laravel.sh -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -b {$this->wordpress_laravel_git_branch} -g {$this->wordpress_laravel_git} -n {$this->name} -j {$os_version} -v {$os} -t {$server} -w {$server_version} -c {$this->action_name} -o {$this->laravel_version} -u {$this->url} -k {$debug} -q {$db_name} -y {$db_user} -i {$db_user_pass}";
	
	   
	   if($debug == "active"){
	     Log::info('Pete ' .$this->action_name . ": " . $command);
		 $this->output = $this->output . "#######".$this->action_name."#######\n";	 
	   }	
	
		putenv("DB_DATABASE=".$db_name);
		putenv("DB_USERNAME=".$db_user);
		putenv("DB_PASSWORD=".$db_user_pass);
		
	   	if($os == "mac"){
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer.phar");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}else if(($os == "linux") || ($os == "olinux")){
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}
		
	   	$this->output = shell_exec($command);
		
	   	$this->save();
	  
	}
	
}
