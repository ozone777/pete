<?php

namespace App;
use App\Site;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\PeteOption;

class Adomain extends Model
{
    //
	public function redirect_to_wordpress(){
		
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$app_root = $pete_options->get_meta_value('app_root');
		$server_conf = $pete_options->get_meta_value('server_conf');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		
		$site = Site::findOrFail($this->site_id); 
		$site_url = $site->url;
		$adomain_name =  $this->name;
		$id = $this->id;
		$filename = "redirect" . $this->id;
		
		chdir("$app_root/Pete/scripts/");
	  	
		$debug = env('DEBUG');
		
		if ($os=="Windows_NT") {
			$command = "{$os}_add_redirect.cmd -v {$os} -n {$adomain_name} -u {$site_url} -r {$app_root} -a {$server_conf} -x {$filename} -j {$os_version} -t {$server} -w {$server_version} -k {$debug}";
		}else {
			$command = "./{$os}_add_redirect.sh -v {$os} -n {$adomain_name} -u {$site_url} -r {$app_root} -a {$server_conf} -x {$filename} -j {$os_version} -t {$server} -w {$server_version} -k {$debug}";
		}
				
		if($debug == "active"){
			Log::info('Pete Add Redirect: ' . $command);
			$site->output = $site->output . "#######ADD REDIRECT#######\n";
		}

		$site->output = $site->output . shell_exec($command);
		$site->save();
		
	}
	
	
    //
	public function delete_redirect_to_wordpress(){
		
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$app_root = $pete_options->get_meta_value('app_root');
		$server_conf = $pete_options->get_meta_value('server_conf');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		
		$site = Site::findOrFail($this->site_id); 
		$site_url = $site->url;
		$id = $this->id;
		$filename = "redirect" . $this->id;
		$adomain_name =  $this->name;
		
		$debug = env('DEBUG');
		
		chdir("$app_root/Pete/scripts/");
		
		if ($os=="Windows_NT") {
			$command = "{$os}_delete_redirect.cmd -v {$os} -n {$adomain_name} -u {$site_url} -r {$app_root} -a {$server_conf} -x {$filename} -j {$os_version} -t {$server} -w {$server_version} -k {$debug}";
		}else {
			$command = "./{$os}_delete_redirect.sh -v {$os} -n {$adomain_name} -u {$site_url} -r {$app_root} -a {$server_conf} -x {$filename} -j {$os_version} -t {$server} -w {$server_version} -k {$debug}";
		}
				
		if($debug == "active"){
			Log::info('Pete Remove Redirect: ' . $command);
			$site->output = $site->output . "#######REMOVE REDIRECT#######\n";
		}
			
		$site->output = $site->output . shell_exec($command);
		$site->save();
	}
}
