<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\User;
use App\Option;
use Log;

class ResetSidebarOptions extends Command
{
    /**
     * The console command name.
     * php artisan resetpassword --option_value=peter301
     * @var string
     */
    protected $signature = 'reset_sidebar_options';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset sidebar options';

    /**
     * Execute the console command.
     *
     * @return void
     */
	
    public function __construct()
    {
        parent::__construct();
    }
	
    public function handle()
    {
	
		$options = Option::where('category', 'sidebar')->get();
	    foreach($options as $option ) {
			$option->delete();
	    }
    }

    
}
