<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Site;
use App\PeteOption;
use App\Backup;
use Log;

class CreateBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     * php artisan create_backup --schedulling=daily
     */
    protected $signature = 'create_backup {--schedulling=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create backup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
	public function handle()
	{
		
		$schedulling = $this->option('schedulling');
		$sites = Site::whereNotNull("barsite_id")->get();
		
		foreach($sites as $site){
		  
			//Delete previous backup
			$backup = Backup::where('site_id', $site->id)->where('schedulling', $this->option('schedulling'))->first();
			if(isset($backup)){
				$backup->delete();
			}
	  	  	
			$site_url = $site->url;
			$site_name = $site->name;
			
			$pete_options = new PeteOption();
			$app_root = $pete_options->get_meta_value('app_root');
			$mysql_bin = $pete_options->get_meta_value('mysql_bin');
			$server_conf = $pete_options->get_meta_value('server_conf');
			$os = $pete_options->get_meta_value('os');
			$app_name = "Wordpress";
			$mysqldump = $mysql_bin . "mysqldump";
	  
			$db_root_pass = env('ROOT_PASS');
			$debug = env('DEBUG');
	  
			$app_name = ucfirst($app_name);
			chdir("$app_root/Pete/scripts/");
	       
			$command="./backup.sh -n {$site_name} -p {$db_root_pass} -r {$app_root} -m {$mysqldump} -v {$os} -w {$app_name} -u {$site_url} -d {$site->id} -k {$debug} -s {$schedulling}";	
			$output = shell_exec($command);
	  	  	
			if(env('APP_DEBUG')){
				Log::info("DESDE BACKUPS");
				Log::info("command:");
				Log::info($command);
				Log::info("output:");
				Log::info($output);
			}
			
			$backup = new Backup();
			$backup->name = $site->name;
			$backup->theme = $site->theme;
			$backup->first_password = $site->first_password;
			$backup->wp_user = $site->wp_user;
			$backup->url =  $site->url;
			$backup->schedulling = $this->option('schedulling');
			$backup->file_name = $site->name."-".$schedulling.".tar.gz";
			$backup->manual = false;
			$backup->site_id = $site->id;
			$backup->save();
	  
		}
	    
	}
}
