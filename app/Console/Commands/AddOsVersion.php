<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Option;

class AddOsVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addosversion {pass?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Os version for the system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		//$this->comment("Hola Pedro {$this->argument('pass')}" );
		
		$option = new Option();

		$option->option_name = "os_version";
        $option->option_value = $this->argument('pass');

		$option->save();
		
    }
}
