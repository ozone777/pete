<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\Option;

class Addoption extends Command
{
    /**
     * The console command name.
     * php artisan dbkeys --var=DB_DATABASE --key=pixma303
     * @var string
     */
    protected $signature = 'addoption {--option_name=} {--option_value=} {--option_category=} {--option_privileges=} {--option_order=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set options for pete';

    /**
     * Execute the console command.
     *
     * @return void
     */
	
    public function __construct()
    {
        parent::__construct();
    }
	
    public function handle()
    {
		
		$option_name = $this->option('option_name');
		$option_name = str_replace("_"," ",$option_name);
		$option_order = $this->option('option_order');
		$option_value = $this->option('option_value');
		$option_category = $this->option('option_category');
		$option_privileges = $this->option('option_privileges');
		
		$option = Option::where('option_name',$option_name)->first();
		
		if($option){
			$option->option_name = $option_name;
	        $option->option_value = $option_value;	
			$option->category = $option_category;
			$option->privileges = $option_privileges;
			$option->order = $option_order;
		}else{
				$option = new Option();
				$option->option_name = $option_name;
		        $option->option_value = $option_value;	
				$option->category = $option_category;
				$option->privileges = $option_privileges;
				$option->order = $option_order;
				$option->visible = true;	
				
		}
		$option->save();	
    }
    
}
