<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\Option;

class Addhex extends Command
{
    /**
     * The console command name.
     * php artisan dbkeys --var=DB_DATABASE --key=pixma303
     * @var string
     */
    protected $signature = 'addhex {--key=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the db hexnumber for secure';

    /**
     * Execute the console command.
     *
     * @return void
     */
	
    public function __construct()
    {
        parent::__construct();
    }
	
    public function handle()
    {
		$var = "HEX_KEY";
		if($this->option('key')){
			$key_value = $this->option('key');
		}else{
			$key_value = bin2hex(openssl_random_pseudo_bytes(32));
		}
		
        $path = base_path('.env');

        if (file_exists($path)) {
			if(env($var) == null){
				$txt = $var."=".$key_value;
				file_put_contents($path, $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
			}else{
            file_put_contents($path, str_replace(
                $var.'='.env($var), $var.'='.$key_value, file_get_contents($path)
            ));
			}
        }

		$this->info($var."=".$key_value);
    }

    
}
