<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Site;
use App\PeteOption;
use App\Backup;
use Log;
use DB;

class DeleteBackups extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     * php artisan delete_backups
     */
    protected $signature = 'delete_backups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete backups';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$pete_options = new PeteOption();
    	$app_root = $pete_options->get_meta_value('app_root');
	  	DB::table('backups')->delete();
	  	shell_exec("rm -rf $app_root/Pete/backups/*");
    }
}
