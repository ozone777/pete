<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Site;
use App\PeteOption;
use App\Backup;
use Log;
use DB;

class ResetSites extends Command
{
   
    protected $signature = 'reset_sites';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Sites';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		//DELETING SITES
		$sites = Site::orderBy('id', 'desc')->get();
	    foreach($sites as $site ) {
	 		$site->delete_wordpress();
			$site->delete();
	    }
		
		//DELETING TRASH
		
		$sites = Site::onlyTrashed()->orderBy('id', 'desc')->get();		
	    foreach($sites as $site ) {
	 		$site->force_delete_wordpress();
	 		$site->forceDelete();
	    }
		
		$users = User::where("admin",false)->get();
	    foreach($users as $user ) {
			
	 		$user->delete();
			
	    }
    }
}
