<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\User;
use App\Option;
use Log;

class ResetUsers extends Command
{
    /**
     * The console command name.
     * php artisan resetpassword --option_value=peter301
     * @var string
     */
    protected $signature = 'reset_users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset users';

    /**
     * Execute the console command.
     *
     * @return void
     */
	
    public function __construct()
    {
        parent::__construct();
    }
	
    public function handle()
    {
	
		User::truncate();
		
    }

    
}
