<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\User;
use App\Option;
use Log;

class ResetLicense extends Command
{
    /**
     * The console command name.
     * php artisan resetpassword --option_value=peter301
     * @var string
     */
    protected $signature = 'reset_license';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset license for pete';

    /**
     * Execute the console command.
     *
     * @return void
     */
	
    public function __construct()
    {
        parent::__construct();
    }
	
    public function handle()
    {
	
		$object =  Option::where('option_name', "token")->first();
		if(isset($object))
			$object->delete();
		
		$object =  Option::where('option_name', "validation_user_email")->first();
		if(isset($object))
			$object->delete();
		
		$object =  Option::where('option_name', "validation_api_key")->first();
		if(isset($object))
			$object->delete();
		
		$object =  Option::where('option_name', "validation_event")->first();
		if(isset($object))
			$object->delete();				
		
    }

    
}
