<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\User;
use Log;

class ResetPassword extends Command
{
    /**
     * The console command name.
     * php artisan resetpassword --option_value=peter301
     * @var string
     */
    protected $signature = 'resetpassword {--option_value=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset password for pete';

    /**
     * Execute the console command.
     *
     * @return void
     */
	
    public function __construct()
    {
        parent::__construct();
    }
	
    public function handle()
    {
	
		$option_value = $this->option('option_value');
		
		$user = User::findOrFail(1);
		
		if($user){
			
			$user->password = bcrypt($option_value);
			$user->save();
			$this->info("The password was successfully changed");
		}
    }

    
}
