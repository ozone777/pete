<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Option;
use App\Secure;

class Addcms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addcms {cms?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Cms to options';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		$cms = $this->argument('cms');
		$option = Option::orderBy('id', 'asc')->where('option_name', '=', "suported_cms")->first();
		
		if(isset($option)){
			if($cms == "reset"){
				$option->option_value = "wordpress";
			}else{
				$option->option_value = $option->option_value . ',' . $cms;
			}
		}else{
			$option = new Option();
			$option->option_name = "suported_cms";
	        $option->option_value = $cms;
		}
		$option->save();
		
    }
}
