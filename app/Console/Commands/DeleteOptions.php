<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\Option;

class DeleteOptions extends Command
{
    /**
     * The console command name.
     * php artisan dbkeys --var=DB_DATABASE --key=pixma303
     * @var string
     */
    protected $signature = 'deleteoptions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'deleteoptions';

    /**
     * Execute the console command.
     *
     * @return void
     */
	
    public function __construct()
    {
        parent::__construct();
    }
	
    public function handle()
    {
		
		$options = Option::orderBy('id', 'desc')->where("visible",false)->get();
		
		foreach ($options as $option){
			$option->delete();
		}
		
    }

    
}
