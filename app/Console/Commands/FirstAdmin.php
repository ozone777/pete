<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Site;

class FirstAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firstadmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert first user to admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		$user = User::findOrFail(1);
		$user->name = "ozone";
		$user->admin = true;
		$user->save();
		
		$sites = Site::orderBy('id', 'desc')->get();
		foreach ($sites as $site) {
			$site->user_id = 1;
			$site->save();
		}
	    
    }
}
