<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;
use App\User;
use App\Site;

class AddUserToSites extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add_user_to_sites';
	#protected $signature = 'addfields {--queue=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add user to sites';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		$user = User::orderBy('created_at', 'asc')->first();
		$sites = Site::orderBy('created_at', 'asc')->get();
		
		foreach ($sites as $site){
			$site->user_id = $user->id;
			$site->save();
		}
		
		$sites_trashed = Site::onlyTrashed()->orderBy('id', 'desc')->get();
		
		foreach ($sites_trashed as $site){
			$site->user_id = $user->id;
			$site->save();
		}
	
    }
}
