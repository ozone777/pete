<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Site;
use Crypt;

class EncryptToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'encrypt_token {--email=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Encrypt token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		$email = $this->option('email');
		
		$user = User::where("email",$email)->first();
		$user->pete_token = Crypt::encrypt($user->pete_token);
		$user->save();
        
    }
}
