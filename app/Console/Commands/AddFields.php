<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;

class AddFields extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addfields {--os=} {--os_version=} {--app_root=} {--server_conf=} {--mysql_bin=}';
	#protected $signature = 'addfields {--queue=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add fields to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		//$this->comment("Hola Pedro {$this->argument('pass')}" );
		
		#DB::table('options')->delete();
		
		$deleted = DB::delete('delete from options where option_name != "db_root_password"');
		
		DB::table('options')->insert(['option_name' => 'app_root', 'option_value' => $this->option('app_root'),'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['option_name' => 'server_conf', 'option_value' => $this->option('server_conf'),'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['option_name' => 'mysql_bin', 'option_value' => $this->option('mysql_bin'),'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['option_name' => 'os', 'option_value' => $this->option('os'),'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['option_name' => 'os_version', 'option_value' => $this->option('os_version'),'created_at' => new DateTime, 'updated_at' => new DateTime]);
		#DB::table('options')->insert(['option_name' => 'url_template', 'option_value' => "t[i].massive.com",'created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		
    }
}
