<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Site;
use Crypt;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	//admin test user VPS
	//php artisan create_admin --name=pedroconsuegrat --email=pedroconsuegrat@gmail.com --pass=pixma301 --pete_token=nBlAY6LhxZqJfvo9Udmz0GIgk5DKO8
    protected $signature = 'create_admin {--name=} {--email=} {--pass=} {--pete_token=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		$email = $this->option('email');
		$pass = $this->option('pass');
		$name = $this->option('name');
		$pete_token = $this->option('pete_token');
		
        User::create([
			'admin' => true,
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($pass),
			'pete_token' => Crypt::encrypt($pete_token)
        ]);
		
		//$user->confirm_password = $pass;
		//$user->save();
	    
    }
}
