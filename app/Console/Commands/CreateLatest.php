<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Site;
use Log;
use App\PeteOption;

class CreateLatest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create_latest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Latest ZIP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		$pete_options = new PeteOption();
		$app_root = $pete_options->get_meta_value('app_root');
		$os = $pete_options->get_meta_value('os');
		
	    chdir("$app_root/Pete/scripts/");
		$command = "./export_latest.sh -r {$app_root} -o {$os}";
		$aux = shell_exec($command);
		Log::info("aux $aux");
		
		
    }
}
