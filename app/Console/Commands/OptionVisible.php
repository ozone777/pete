<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\Option;

class OptionVisible extends Command
{
    /**
     * The console command name.
     * php artisan dbkeys --var=DB_DATABASE --key=pixma303
     * @var string
     */
    protected $signature = 'optionvisible';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'optionvisible';

    /**
     * Execute the console command.
     *
     * @return void
     */
	
    public function __construct()
    {
        parent::__construct();
    }
	
    public function handle()
    {
		
		$options = Option::orderBy('id', 'desc')->get();
		
		foreach ($options as $option){
			$option->visible = true;
			$option->save();
		}
		
    }

    
}
