<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\PeteOption; 

class Kernel extends ConsoleKernel
{
    /****
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
		\App\Console\Commands\AddPass::class,
		\App\Console\Commands\Dbkeys::class,
		\App\Console\Commands\Addhex::class,
		\App\Console\Commands\ResetPassword::class,
		\App\Console\Commands\Addoption::class,
		\App\Console\Commands\Addcms::class,
		\App\Console\Commands\AddOsVersion::class,
		\App\Console\Commands\AddFields::class,
		\App\Console\Commands\DeleteOptions::class,
		\App\Console\Commands\ResetSites::class,
		\App\Console\Commands\OptionVisible::class,
		\App\Console\Commands\FirstAdmin::class,
		\App\Console\Commands\AddTemplate::class,
		\App\Console\Commands\AddApacheVersion::class,
		\App\Console\Commands\CreateAdmin::class,
		\App\Console\Commands\CreateBackup::class,
		\App\Console\Commands\DeleteBackups::class,
		\App\Console\Commands\CreateLatest::class,
		\App\Console\Commands\ResetLicense::class,
		\App\Console\Commands\ResetUsers::class,
		\App\Console\Commands\AddUserToSites::class,
		\App\Console\Commands\EncryptToken::class,
		\App\Console\Commands\UpdateUser::class,
		\App\Console\Commands\ResetSharedUsers::class,
		\App\Console\Commands\ResetSidebarOptions::class,
		\App\Console\Commands\Removeoption::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {	
		$schedule->command("create_backup --schedulling=weekly")->weekly();
		$schedule->command("create_backup --schedulling=daily")->daily();
	
    }
}
