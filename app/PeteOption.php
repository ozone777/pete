<?php

namespace App;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Option;

class PeteOption extends Model
{
    //
    //
	public function __construct(){
		global $options;
		
			$options = Option::all()->keyBy('option_name');
		
	}
	
	public function get_meta_value($meta_key){
		global $options;
		if(isset($options[$meta_key])){
			return $options[$meta_key]->option_value;
		}else{
			return "";
		}
		
	}
	
	public function get_meta_date($meta_key){
		global $options;
		if(isset($options[$meta_key])){
			return $options[$meta_key]->option_date;
		}else{
			return "";
		}
		
	}
	
}
