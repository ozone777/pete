<?php

namespace App;
use Log;
use Illuminate\Database\Eloquent\Model;
use App\Site;
use App\PeteOption;

class Backup extends Model
{
    //
	
	public function generate_uniq_name(){
		
		$clean_name = $this->name;
		
		if (strpos($this->name, '_') !== false) {
			$aux = $this->name;
			$pos = strpos($aux, "_");
			$str1 = substr($aux,0,$pos+1);
			$clean_name = str_replace($str1,"",$aux);
		}
		
		$uniqid = uniqid();
		$uniqid = "restore".$uniqid."_".$clean_name;
		return $uniqid;
	}
	
	public function restore_from_backup(){
		
		$pete_options = new PeteOption();
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$apache_version = $pete_options->get_meta_value('apache_version');
			
	    $db_name = "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
	    $db_user = "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
	    $db_user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		
	    $wpkey1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey3 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey4 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey5 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey6 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey7 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey8 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
		
		$file_route = "$app_root/Pete/backups/$this->file_name";
		$zip_file_url = "$app_root/Pete/backups/$this->file_name";
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = "Wordpress";	
		$db_root_pass = env('ROOT_PASS');
		$debug = env('DEBUG');	
		
		$site = Site::find($this->site_id);
		
		$new_site = $site->replicate();
		$new_site->name = $site->name."restored".$this->id;
		$new_site->save();
		$id = $new_site->id;
		
		$site->massive_delete();
		$site->delete();
	   
	  $command = "./import.sh -n {$new_site->name} -u {$new_site->url} -z {$zip_file_url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -x {$db_name} -y {$db_user} -l {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -w {$app_name} -s {$id} -j {$os_version} -t {$apache_version} -o {$file_route} -k {$debug}" ;
	  
	  chdir("$app_root/Pete/scripts/");
	  $output = shell_exec($command);
	  
	  if($debug == "active"){
		  Log::info('restore_from_backup Command: ' . $command);
		  $new_site->output = $output;
		  $new_site->save();
	  }
	  
	}
	
	
}
