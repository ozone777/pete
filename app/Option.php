<?php

namespace App;
use Log;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    //
	
	public static function delete_meta_value($key){
		$object = Option::where('option_name',$key)->first();
		Log::info('Entro en delete_meta_value');
		if($object){
		   $object->delete();
		   Log::info('Entro en borrado');
		}
	}
	
	public static function get_meta_value($key){
		$object = Option::where('option_name',$key)->first();
		if(isset($object)){
			return $object->option_value;
		}else{
			return "";
		}		
	}
}
