<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Backup;
use DB;
use Log;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password','admin','pete_token', 'shared', 'user_db', 'user_db_pass','restored','snapshot'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
	
   public function sites()
     {
         return $this->hasMany('App\Site');
     }
	 
   public function my_sites(){
	   if($this->admin){
	   	$sites = Site::orderBy('id', 'desc');
	   }else{
	   	 $sites = Site::orderBy('id', 'desc')->where("user_id",$this->id);
	   }	
	return $sites;
   }
   
    public function my_trash_sites(){
 	   if($this->admin){
 	   $sites = Site::onlyTrashed()->orderBy('updated_at', 'desc');
 	   }else{
 	   	 $sites = Site::onlyTrashed()->where("user_id",$this->id)->orderBy('updated_at', 'desc');
 	   }	
		return $sites;
	}
	
	public function my_backups(){
		
		if($this->admin){
			
			$backups = Backup::orderBy('backups.created_at', 'desc')
				    ->select(DB::raw('backups.id, backups.schedulling, backups.file_name,backups.name, backups.url, backups.created_at, sites.barsite_id, sites.wp_user, sites.theme'))
					->join('sites', 'sites.id', '=', 'backups.site_id')
					->where("manual",false);	
			
		}else{
			$backups = Backup::orderBy('backups.created_at', 'desc')
				    ->select(DB::raw('backups.id, backups.schedulling, backups.file_name,backups.name, backups.url, backups.created_at, sites.barsite_id, sites.wp_user, sites.theme'))
					->join('sites', 'sites.id', '=', 'backups.site_id')
					->where("sites.user_id",$this->id)->where("manual",false);	
		}
	   	return $backups;	
	}
	
	public function my_snapshots(){
		
		 if($this->admin){
			 
 	 		$backups = Backup::orderBy('backups.created_at', 'desc')
 	 			    ->select(DB::raw('backups.id, backups.schedulling, backups.file_name,backups.name, backups.url, backups.created_at, sites.barsite_id, sites.wp_user, sites.theme'))
 	 				->join('sites', 'sites.id', '=', 'backups.site_id')
 	 				->where("manual",true);
			 
		 }else{
	 		$backups = Backup::orderBy('backups.created_at', 'desc')
	 			    ->select(DB::raw('backups.id, backups.schedulling, backups.file_name,backups.name, backups.url, backups.created_at, sites.barsite_id, sites.wp_user, sites.theme'))
	 				->join('sites', 'sites.id', '=', 'backups.site_id')
	 				->where("sites.user_id",$this->id)->where("manual",true);
		 }
		
		 return $backups;
	}
	
	public function has_one_site(){
		$site = Site::where("user_id",$this->id)->get()->first();
		if(isset($site)){
			return true;
		}else{
			return false;
		}
	}
	
	public function can_create_snapshot(){
		
		if($this->shared == true){
			if($this->snapshot == true){
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	
	public function can_create_restore(){
		
		if($this->shared == true){
			if($this->restored == true){
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
   
}
