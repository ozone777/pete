## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

#write out current crontab
crontab -l > mycron
#echo new cron into cron file
echo "* * * * * php /data/www/Pete && php artisan schedule:run" >> mycron

#Testing
php artisan reset_sites
php artisan reset_users
Run test katelon

#install new cron file
crontab mycron
rm mycron

Linux variables
php artisan dbkeys --var=phpmyadmin --key=/phpmyadmin
php artisan dbkeys --var=server_version --key=2.4.33
php artisan dbkeys --var=server --key=apache
php artisan dbkeys --var=validation_url --key=https://dashboard.wordpresspete.com
php artisan dbkeys --var=version --key=1.1
php artisan dbkeys --var=server_conf --key=/usr/local/apache/conf/vhost
php artisan dbkeys --var=os_version --key=ubuntu18
php artisan dbkeys --var=os --key=linux
php artisan dbkeys --var=app_root --key=/data/www	

Mac variables
php artisan dbkeys --var=domain_template --key=test
php artisan dbkeys --var=phpinfo --key=http://phpinfo.test
php artisan dbkeys --var=phpmyadmin --key=http://phpmyadmin.test
php artisan dbkeys --var=server_version --key=2.4.37
php artisan dbkeys --var=server --key=apache
php artisan dbkeys --var=validation_url --key=https://dashboard.wordpresspete.com
php artisan dbkeys --var=version --key=1.1
php artisan dbkeys --var=server_conf --key=/usr/local/etc/httpd/extra/vhost
php artisan dbkeys --var=os_version --key=10.13.5
php artisan dbkeys --var=os --key=mac
php artisan dbkeys --var=app_root --key=/Users/pedroconsuegra/Sites

Windows variables
php artisan dbkeys --var=domain_template --key=test
php artisan dbkeys --var=phpinfo --key=http://phpinfo.test
php artisan dbkeys --var=phpmyadmin --key=http://phpmyadmin.test
php artisan dbkeys --var=server_version --key=2.4.37
php artisan dbkeys --var=server --key=apache
php artisan dbkeys --var=validation_url --key=https://dashboard.wordpresspete.com
php artisan dbkeys --var=version --key=1.1
php artisan dbkeys --var=server_conf --key=/usr/local/etc/httpd/extra/vhost
php artisan dbkeys --var=os_version --key=10.13.5
php artisan dbkeys --var=os --key=mac
php artisan dbkeys --var=app_root --key=/Users/pedroconsuegra/Sites

