<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;


class OsxSeeder extends Seeder {

    public function run()
    {
			
		DB::table('options')->delete();
		DB::table('options')->insert(['id' => '1','option_name' => 'db_root_password', 'option_value' =>'root','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '2','option_name' => 'app_root', 'option_value' => '/Applications/MAMP/htdocs','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '3','option_name' => 'server_conf', 'option_value' => '/Applications/MAMP/conf/apache/extra/httpd-vhosts.conf','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '4','option_name' => 'mysql_bin', 'option_value' => '/Applications/MAMP/Library/bin/','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '5','option_name' => 'apache_bin', 'option_value' => '/Applications/MAMP/Library/bin/','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '6','option_name' => 'os', 'option_value' => 'osx','created_at' => new DateTime, 'updated_at' => new DateTime]);	
		DB::table('options')->insert(['id' => '7','option_name' => 'server', 'option_value' => 'apache','created_at' => new DateTime, 'updated_at' => new DateTime]);		
		
    }
	
	

}