<?php

use Illuminate\Database\Seeder;

use Laracasts\TestDummy\Factory as TestDummy;

class MassiveSeeder extends Seeder {

    public function run()
    {
		
		DB::table('actions')->delete();
		DB::table('actions')->insert(['id' => '1','name' => 'New', 'body' => 'Create new App', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('actions')->insert(['id' => '2','name' => 'Clone', 'body' => 'Clone your App', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('actions')->insert(['id' => '3','name' => 'Import', 'body' => 'Import your App', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		
    }

}