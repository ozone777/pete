@extends('layout')

@section('header')

<style>
	
	.button {
	    color: #0071a1;
	    border-color: #0071a1;
	    background: #f3f5f6;
	    vertical-align: top;
		display: inline-block;
		text-decoration: none;
		font-size: 13px;
		line-height: 2.15384615;
		min-height: 30px;
		margin: 0;
		padding: 0 10px;
		cursor: pointer;
		border-width: 1px;
		border-style: solid;
		-webkit-appearance: none;
		border-radius: 3px;
		white-space: nowrap;
		box-sizing: border-box;
		margin-left: 10px;
		margin-bottom: 15px;
		
	}
	
</style>

@endsection

@section('content')

	
	  <div class="row">
		  
		  <div class="col-md-12">
			  <br />
	 		 <pre></pre>
			 
		 </div>
	</div>
	

	  <div class="row">
		  
		  <div class="col-md-6">

	 		 <h3>Add Plugins</h3>
			 
		 </div>
	</div>
	
	
	<div id="plugins_area">
		
		
		
	</div>	
	
	  
	  <div class="row">
		  
		  <div class="col-md-6">

	 		 <h3>WordPress Plugins</h3>
			 
		 </div>
	</div>
	  
	  
    <div class="row">
		
        <div class="col-md-12">
			
            	 <i class="pete">Quickly and easily export your WordPress into WordpressPete format.</i> <br /><br />
				 
				 <a class="btnpete" href="/plugins/pete-converter.zip"><i class="glyphicon glyphicon-download-alt"></i> Download Pete Converter Plugin</a>
				 
				 <br /><br />
				 
		</div>
	</div>
	
	
    <div class="row">
		
        <div class="col-md-12">
			
			
            	 <i class="pete">Get the best of both worlds by integrating WordPress with Laravel – a powerful MVC framework for PHP.</i><br /><br />
				 
				 <a class="btnpete" href="/plugins/laravel-integration-by-pete.zip"><i class="glyphicon glyphicon-download-alt"></i> Download Laravel Integration by Pete Plugin</a>
				 <br /><br />
		</div>
	</div>
	
	
    <div class="row">
		
        <div class="col-md-12">
			
					 <br /><br />
		</div>
	</div>
	
	
<script>
	
	
$(document).ready(function(){
		
	html ="";
		
	$.ajax({
				url: "{{$dashboard_url}}/plugins_json",
				dataType: 'JSONP',
				type: 'GET',
				success : function(result) {
				    
					cont = 0;
					for (var item in result) {
						
						if(cont%2 == 0){
							html+='<div class="row">';
						}
						
							html+='<div class="col-md-6">';
							html+='<div class="card">';
							html+='<img src="/wordpress_laravel_plugin_image.jpg" width="100%">';
							html+='<div class="card-body">';
							html+='<h5 style="padding: 10px 10px 10px 10px"; class="card-title">'+result[item].title+'</h5>';
							html+='<p style="padding: 10px 10px 10px 10px" class="card-text">';
							html+=result[item].description;
							html+="</p>"
							
							@if($pete_options->get_meta_value('os') == "mac")
            				
							html+='<form action="/pete_plugins_install" method="POST">';
               			 	html+='<input type="hidden" name="install_script" value="'+result[item].mac_install_script+'">';
							html+='<button type="submit" class="button">Install Now</button>';
							html+='</form>';
							
            				html+='<form action="/pete_plugins_uninstall" method="POST">';
               			 	html+='<input type="hidden" name="uninstall_script" value="'+result[item].mac_uninstall_script+'">';
							html+='<button type="submit" class="button">Uninstall</button>';
							html+='</form>';
							
            				html+='<form action="/pete_plugins_update" method="POST">';
               			 	html+='<input type="hidden" name="update_script" value="'+result[item].mac_update_script+'">';
							html+='<button type="submit" class="button">Update</button>';
							html+='</form>';
							
							@elseif($pete_options->get_meta_value('os') == "olinux")
							
							html+='<form action="/pete_plugins_install" method="POST">';
               			 	html+='<input type="hidden" name="install_script" value="'+result[item].olinux_install_script+'">';
							html+='<button type="submit" class="button">Install Now</button>';
							html+='</form>';
							
            				html+='<form action="/pete_plugins_uninstall" method="POST">';
               			 	html+='<input type="hidden" name="uninstall_script" value="'+result[item].olinux_uninstall_script+'">';
							html+='<button type="submit" class="button">Uninstall</button>';
							html+='</form>';
							
            				html+='<form action="/pete_plugins_update" method="POST">';
               			 	html+='<input type="hidden" name="update_script" value="'+result[item].olinux_update_script+'">';
							html+='<button type="submit" class="button">Update</button>';
							html+='</form>';
							
							@elsif($pete_options->get_meta_value('os') == "win")
							
							
							@endif
							
							html+='</form>';
							html+='</div>';
							html+='</div>';
							html+='</div>';
						
							cont++;
							
						if(cont%2 == 0){	
							html+='</div>'
						}
					}
					
					$("#plugins_area").append(html);
					
				},
			
			
			error: function(xhr, resp, text) {
			
	            console.log(xhr, resp, text);
				//location.reload();
	        }
				
					
		});	
});	
				
	
</script>

@endsection