<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	
	<title>WordPressPete</title>
	
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>

    <!--  Custom CSS    -->
    <link href="/assets/css/custom.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
	
    <!--   Core JS Files   -->
	<script
	  src="https://code.jquery.com/jquery-1.10.2.min.js"
	  integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg="
	  crossorigin="anonymous"></script>
	<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
	
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.35.4/js/bootstrap-dialog.min.js" type="text/javascript"></script>
	
	<script src="/js/jquery.observe_field.js"></script>
	
	<script src="/js/jquery.validate.min.js"></script>

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="teal" data-image="/assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo"><img style="display: inline; width: 180px" src="/pete_header.png">
				
				 v&nbsp;{{$pete_options->get_meta_value('version')}}</div>
			
	

            <ul class="nav">
				
				 @foreach($sidebar_options as $option)
				 
					@if($viewsw == $option->option_value)
					   <li class="active">
					@else
					   <li>
					@endif
	                    <a href="{{$option->option_value}}">
	                        <i class="pe-7s-browser"></i>
	                        <p>{{$option->option_name}}</p>
	                    </a>
	                </li>
				 
				 @endforeach
				
               
            </ul>
			
			<div id="loading_area">
				
				
				
			</div>
			
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
 						
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
						
						
                       
                        <li>
                            <a class="btnpeteout" href="/auth/logout">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">

		        @yield('header')
		        @yield('content')

            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
					
					
                     
				   <p style="padding-top: 5px; font-size:12px;">
					   
					   @if(isset($apache_v_dialog))
                         {{$apache_v_dialog}}
					   @endif
					   
					   <br />
					   @if(isset($php_v_dialog))
                         {{$php_v_dialog}}
					   @endif
					  
					   <br />
					   @if(isset($mysql_v_dialog))
                         {{$mysql_v_dialog}}
					   @endif
					 
                     </p>
				  
				
                    
                </nav>
                <p class="copyright pull-right">
					 <a href="http://wordpresspete.com">wordpresspete.com</a>
                    
                </p>
            </div>
        </footer>

    </div>
</div>



</body>

 

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="/assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="/assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    -->
		
    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="/assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="/assets/js/demo.js"></script>
	
	<script type="text/javascript">
		
	///////
	function activate_webistebar_loader()
	{
			//Add Loader
		    $("#loadMe").modal({
		      backdrop: "static", //remove ability to close modal with click
		      keyboard: false, //remove option to close with keyboard
		      show: true //Display loader!
		    });
	}		
	
	$( document ).ajaxStart(function() {
		
		activate_webistebar_loader();
		
	});
	
	$(document).ajaxSuccess(function() {
	
	  $("#loadMe").modal("hide");
	  
	});
	
	$("#create_button").click(function() {
  	  	activate_webistebar_loader();
	});

	</script>
	
	
	
	<!-- Modal -->
	<div class="modal fade" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="loadMeLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-body text-center">
	        <div class="loader"></div>
	       
	      </div>
	    </div>
	  </div>
	</div>
	
	
	
</html>
