@extends('layout')

@section('header')
<div class="page-header">
	<h3> Create WordPress</h3>
	
	<div id="loading_area"></div>
	
</div>
	
@endsection

@section('content')
@include('error')

   
			
<form action="{{ route('sites.store') }}" id ="SiteForm" method="POST" enctype="multipart/form-data">

	<input type="hidden" name="_token" value="{{ csrf_token() }}">          
	<div class="row">
		<div class="col-md-12">
									
									
			@if(isset($error))
			
			<div class="alert alert-danger">
				<p>There were some problems with your input.</p>
				<ul>
					@if($site->error_message1)
					<li><i class="glyphicon glyphicon-remove"></i>{{$site->error_message1}}</li>
					@endif
							
					@if($site->error_message2)
					<li><i class="glyphicon glyphicon-remove"></i>{{$site->error_message2}}</li>
					@endif
							
				</ul>
			</div>
			
			@endif
				
			
				 
		</div>
	</div>
							 
							
							
				
	<div class="row">
		<div class="col-md-12">
								
						
			<select class="form-control" id="action_name-field" name="action_name" required="">
				<option value="">Select Action</option>
				<option value="New">New</option>
				<option value="Clone">Clone</option>
				<option value="Import">Import</option>
						
			</select>
							
			<br />
						
		</div>
						
	</div>
							
							
				
	<div class="row">
		<div class="col-md-12">
									
			<div id="project_to_clone">
					
			</div>
					
			<div class="form-group" id="name_div" style="display: none;">
				<p> Project name</p>
				<input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}" required/>
                   
				<div id="name_error_area"> 
				</div>
			</div>
				
		</div>
	</div>
				
				
	<div class="row">
		<div class="col-md-12">
			<div class="form-group" id="import_from_git" style="display: none;">
				
				<p><input type="checkbox" id="import_from_git_checkbox" name="import_from_git_checkbox"> Import from GIT</p>
 			   
			</div>
				
		</div>
	</div>
				
						    
                
							
	@if($pete_options->get_meta_value('domain_template'))
							
						    
	<div class="row">
		<div class="col-md-12">
						
			<div id="url_div" style="display: none;">
				<p>URL</p>
				<input type="text" id="url-field" name="url" class="inline_class url_wordpress_laravel" required/>
				<div id="url_wordpress_helper" class="inline_class">.{{$pete_options->get_meta_value('domain_template')}}</div>
				 
			</div>
			<br />
		</div>
				
	</div>
							
	@else
						  
	<div class="row">
		<div class="col-md-12">
									
			<div class="form-group" id="url_div" style="display: none;">
				<p>URL</p>
				<input type="text" id="url-field" name="url" class="form-control " value="{{ old("url") }}" required/>
					   
				<div id="url_error_area"> 
				</div>
					   
			</div>
		</div>
	</div>
						  
	@endif
							
							
				
				
			    
				
				
						   
				
	<div class="row">
		<div class="col-md-12">
									
									
			<div class="form-group" id="zip_file_url_div" style="display: none;">
				<label for="zip_file_url-field">Upload Pete tar.gz file</label>
				<input type="file" id="filem" name="filem">
			</div>
						
			<div id="big_file_container" style="display: none;"><input type="checkbox" id="big_file" name="url_template"  value="true"> &nbsp; File path for large files (Optional)</div>
							
			<label id="label_big_file_container" style="display: none;">Insert the complete route to Pete tar.gz file</label>
			<input type="text" id="big_file_route" name="big_file_route" style="display: none;" class="form-control"/>
				
			<br/>
					
				            
				
		</div>
	</div>
				
							
	<div id="massive_form">
		
	</div>
                    
               
	<button type="submit" id="create_button" class="btnpete">Create</button>
	<br /><br />
</form>
			
			
			

        
	
<script>
	
	var patt = new RegExp(/^[a-z0-9]+$/i);
	var patturl = new RegExp(/^[a-z0-9\.]+$/i);
	
	$("#url_template").change(function() {
		if($("#url-field").prop("readonly")){
			$("#url-field").prop("readonly", false);
		}else{
			$("#url-field").prop("readonly", true);
		}
		//alert("hola");
	});
	
	$("#big_file").click(function() {
		
		//alert("hi big");
		$("#big_file_route").toggle();
	});
	
	$("#action_name-field").change(function() {
		form_logic($("#app_name-field").val(),$("#action_name-field").val());
	});
	
	$("#import_from_git_checkbox").change(function() {
		$("#wordpress_laravel_git").toggle();
		$("#wordpress_laravel_git_branch").toggle();
	});
	
	function wordpress_laravel_select(){
		$("#wordpress_laravel_target-field").change(function() {
			$("#url_wordpress_laravel_helper").html("."+$(this).find('option:selected').text());
		});
	}
				
	function hide_all(){
		$("#url_div_wordpress_laravel").hide();
		$("#url_div_wordpress_helper").hide();
		$("#name_div").hide();
		$("#wordpress_laravel_git").hide();
		$("#wordpress_laravel_git_branch").hide();
		$("#big_file_container").hide();
		$("#big_file").hide();
		$("#big_file_route").hide();
		$("#name_div").hide();
		$("#url_div").hide();
		$("#wordpress_laravel_target-field").hide();
	}
	
	function form_logic(app,action){
		if((app != 0) && (action != 0)){
			hide_fields();
			//$("#name_div").show();
			
			console.log(app);
			console.log(action);
			
				
				if(action=="New"){
					$("#name_div").show();
					$("#url_div").show();	
					$("#massive_form").html("");
				}
				else if (action=="Clone") {
			
						$.ajax({
							url: "/sites/get_sites",
							type: "get",
							datatype: 'json',
							data: { app_name: "Wordpress"},
							success: function(data){
								$("#loading_area").html('');
								console.log(data);
								select_aux = "<p>Project to clone</p>" ;
								select_aux += '<select id="to_clone_project_id-field" name="to_clone_project_id" class = "form-control">';	
							
								var arrayLength = data.length;
								for (var i = 0; i < arrayLength; i++) {	
									select_aux +='<option value="'+data[i].id+'">'+data[i].url+'</option>';
								}
							
								select_aux +='</select><br/>';
								$("#name_div").show();
								$("#url_div").show();
								$("#project_to_clone").show();
								$("#project_to_clone").html(select_aux);
								$("#massive_form").show();
							}
						
						});
									 
				}else if (action=="Import") {
					$("#name_div").show();
					$("#url_div").show();
					$("#zip_file_url_div").show();
					$("#big_file").show();
					$("#big_file_container").show();
				  
				}
				
			}
	
	}
	
	
	
	function hide_fields(){
				
		$("#url_div_wordpress_laravel").hide();
		$("#url_div_wordpress_helper").hide();
		$("#name_div").hide();
		$("#wordpress_laravel_git").hide();
		$("#wordpress_laravel_git_branch").hide();
		$("#big_file_container").hide();
		$("#big_file").hide();
		$("#big_file_route").hide();
		$("#name_div").hide();
		$("#url_div").hide();
		$("#wordpress_laravel_target-field").hide();
				
		$("#massive_form").hide();
		$("#name_div").hide();
		$("#url_div").hide();
		$("#zip_file_url_div").hide();
		$("#db_root_pass_div").hide();
	  
		$("#url_div_wordpress_laravel").hide();
		$("#url_div_wordpress_helper").hide();
		$("#wordpress_laravel_git").hide();
		$("#wordpress_laravel_git_branch").hide();
				  
		$("#project_to_clone").hide();
				  
	}
				
			
				
	var createsw = false;
	 
	jQuery( document ).ready(function( $ ) {

		$("#SiteForm").validate();
	 
	});
	  
	  
	$("#create_button").click(function() {
	  	  
		//Aditional javascript validations for import action
		   
		app_name = $("#app_name-field").val();
		action_name = $("#action_name-field").val();
		  
		//Import CMS case
		if(((app_name == "Wordpress") || (app_name == "Drupal")) & (action_name == "Import")){
		  	  
			console.log("validations CMS");
			console.log($("#import_from_git_checkbox").is(":checked"));
			console.log($('input[type=file]').val());
			  
			if(($('input[type=file]').val() == "") & ($("#big_file_route").val() == "")){
				alert("Please select a pete .tar.gz file")
				return false
			}

			//Import wordpress+laravel case
		}else if((app_name == "Wordpress+laravel") & ((action_name == "Import") || (action_name == "Clone"))){
			console.log("Validations wordpress+laravel");
			console.log($('input[type=file]').val());
			 
						 
		}
	});
				  
				  
				  
	//Wordpress+laravel input logic
	$(document).ready(function(){
					  
		$('#name-field').keyup(function(e){
			$('#wordpress_laravel_name-field').val($('#name-field').val());
		});
					  
		@if($pete_options->get_meta_value('domain_template'))
					  
		$('#name-field').keyup(function(e){
			$('#url-field').val($('#name-field').val());
		});
					  
		@endif
					  
		$('#wordpress_laravel_git-field').keyup(function(e){
						  
			var n = $(this).val().startsWith("https://");
			if(n==true){
				$(this).removeClass("text_area_fancy_error").addClass("text_area_fancy_done");
				console.log("nice");
			}else{
				$(this).removeClass("text_area_fancy_done").addClass("text_area_fancy_error");
							 
				console.log("bad");
			}
		});
					  
	});
		
	
	</script>


	
	@endsection