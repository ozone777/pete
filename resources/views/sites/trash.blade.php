@extends('layout')

@section('header')
   
@endsection

@section('content')

    <div class="row">
		
        <div class="col-md-12">
				<br />
		</div>
	</div>
	
	 @include('sites/_table_header')
	 
	 

    <div class="row">
		
        <div class="col-md-12">
			
			<div class="content table-responsive">
				
            @if($sites->count())
                <table style="padding-left: 10px; padding-right: 10px;" class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                           <th>Name</th>
                        <th>Url</th>
                        
                        <th>Action</th>
						<th>App</th>

                         <th class="text-right">Options</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($sites as $site)
                            <tr>
                                <td>{{$site->id}}</td>
                     <td>{{$site->name}}</td>           
                    <td>
					<a href="http://{{$site->url}}" target ='_blank'>{{$site->url}}</a>
					</td>
                    
                    <td>{{$site->action_name}}</td>
					<td>{{$site->app_name}}</td>
					
                                <td class="text-right">
                                    
									
									@if($site->app_name != "WordPressPlusLaravel" )
								    	<a class="option_button" href="/sites/restore?id={{$site->id}}"></i>Restore</a>
									@endif
									
									
									@if($current_user->admin)
									
                                    <form action="/force_delete" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="site_id" value="{{$site->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="option_button" style="background-color: #f1592a; width: 100%">Force delete</button>
                                    </form>
									
									
									 @endif
									
                                   
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $sites->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif
			</div>
        </div>
    </div>

@endsection