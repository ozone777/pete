@extends('layout')

@section('header')


		<div class="row">
	            
	        <div class="col-md-6">
    
        		<h1>
            		<img alt="w00t!" src="/pete.png" style="height: 204px">
			
					<p style="font-size: 13px; ">WordPress efficiency starts here.</p>
			
        		</h1>
		
			</div>
		
		 <div class="col-md-6">
			 <h1>
				 <a class="btnpete pull-right" href="{{ route('sites.create') }}"><i class="glyphicon glyphicon-plus"></i> Create New WordPress</a>
			 </h1>
		</div>
		
		</div>


@endsection

@section('content')


	@include('sites/_table_header')
	
    <div class="row">
        <div class="col-md-12">
			<div class="content table-responsive">
            @if($backups->count())
                <table style="padding-left: 10px; padding-right: 10px;" class="table table-hover table-striped">
                    <thead>
                        <tr>
                        <th>Project Name</th>
                        <th>Url</th>
                        <th>Label</th>
						<th>Filename</th>
                         <th class="text-right">Options</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($backups as $backup)
                            <tr>
                     		   <td>{{$backup->name}}</td>           
                    		   <td><a href="http://{{$backup->url}}" target ='_blank'>{{$backup->url}}</a></td>  
							   <td>{{$backup->schedulling}}</td>
							   <td>{{$backup->file_name}}</td>
							   
                                <td class="text-right">
									
									<a class="option_button restore_backup" backup_url="{{$backup->url}}" backup_id="{{$backup->id}}" href="#">Restore</a>
									
                                    <form action="/delete_backup" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
										
                                      <input type="hidden" name="_method" value="POST">
									  <input type="hidden" name="view" value="snapshots">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
										
										<input type="hidden" name="backup_id" value="{{$backup->id}}">
										
                                        <button type="submit" class="option_button" style="background-color: #f1592a; width: 100%">Delete</button>
                                    </form>
                                   
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            
            @else
               <table style="padding-left: 10px; padding-right: 10px;" class="table table-hover table-striped">
                    <thead>
                        <tr>
	                        <tr>
	                        <th>Id</th>
	                        <th>Project Name</th>
	                        <th>Url</th>
	                        <th>Label</th>
	                         <th class="text-right">Options</th>
	                        </tr>
                        </tr>
                    </thead>

                    <tbody>
					</tbody>
					
				</table>
            @endif
			</div>
        </div>
    </div>
	
	
	
	<div class="modal fade">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h4 class="modal-title">Modal title</h4>
	      </div>
	      <div class="modal-body">
			<form id="site_form" action="/restore_backup" style="display:none" method="POST">
				<input type="hidden" name="_method" value="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">         
				
				<input type="hidden" id="backup_id_form" name="backup_id_form" value="">
				<input type="hidden" id="domain_form" name="domain_form" value="">
				<input type="hidden" id="backup_action_form" name="backup_action_form" value="">
				
			    <input type="submit" />
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	
	
	<script type="text/javascript">
	
	
	   
	
    	$(document).ready(function(){
			
  			@if(isset($exporturl))
				console.log("download url: {{$exporturl}}");
  		  		window.location.assign("/{{$exporturl}}");
  		
  		  @endif
				
			
			$(".restore_backup").click(function() {
				
				backup_id = $(this).attr("backup_id");
				backup_url = $(this).attr("backup_url");
				
				html ='';
				html +='<select name="backup_action" id="backup_action">';
				html +='<option value="restore_to_new_domain">Restore to new domain</option>';
				html +='<option value="restore_to_current_domain">Restore to current domain: '+backup_url+'</option>';
				html +='</select> ';
				html +='<input id="domain" name="domain" value="">';
				
			    BootstrapDialog.show({
			          title: 'Restore Site',
			          message: html,
			          buttons: [{
			              label: '<a class ="btnpete">Restore Site</a>',
			              action: function(dialog) {
			                  // submit the form
							  //snapshot_label = $("#snapshot_label").val();
							  
							  $("#backup_id_form").val(backup_id);
							  $("#domain_form").val($("#domain").val());
							  $("#backup_action_form").val($("#backup_action").val());
							  
							//  $("#snapshot_label_form").val(snapshot_label);
							$('#site_form').submit();
							
							dialog.close();
							activate_webistebar_loader();
							
			              }
			          }]
			      });
				
			});
			
        	//demo.initChartist();
			/*
        	$.notify({
            	icon: 'pe-7s-arc',
            	message: "Welcome to the future of OZONE. Welcome to <b>Massive Server</b>"

            },{
                type: 'info',
                timer: 4000
            });
			*/
			
			
		   @if(isset($success))
			
			@if($success == "true")
			
		    var delayInMilliseconds = 3000; //1 second

		    setTimeout(function() {
		      //your code to be executed after 1 second
		   	 $("#loadMe").modal("hide");
		    }, delayInMilliseconds);

		     $.ajax({
		           url: "/reload_server",
		           type: "get",
		           datatype: 'json',
				   data: {site_id : "{{$site_id}}}"},
		           success: function(data){
		            // alert("success");	
	
		           }
		
		     });

		    @endif
			 
		  @endif	
		   

    	});
		
		
	</script>
	

@endsection