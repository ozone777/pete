	<div class="row">
				
					<div class="col-md-12">
						
						<a class="table_tab" href="/sites">Sites</a>
						
	                    <a class="table_tab" href="/sites/trash">Trash</a>
						
						@if($pete_options->get_meta_value('backups') == "on")
						
						<a class="table_tab" href="/sites/backups">Backups</a>
						
						@endif
						
						 @if($pete_options->get_meta_value('snapshots') == "on")
							<a class="table_tab" href="/sites/snapshots">Snapshots</a>
						@endif
					
					</div>
				
	</div>