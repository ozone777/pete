@extends('layout')

@section('header')
   
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('options.update', $option->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('option_name')) has-error @endif">
                       <label for="option_name-field">Option_name</label>
                    <input type="text" id="option_name-field" name="option_name" class="form-control" value="{{ $option->option_name }}"/>
                       @if($errors->has("option_name"))
                        <span class="help-block">{{ $errors->first("option_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('option_value')) has-error @endif">
                       <label for="option_value-field">Option_value</label>
                    <input type="text" id="option_value-field" name="option_value" class="form-control" value="{{ $option->option_value }}"/>
                       @if($errors->has("option_value"))
                        <span class="help-block">{{ $errors->first("option_value") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('options.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection