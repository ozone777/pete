@extends('layout')

@section('header')
  
@endsection

@section('content')
			
        <div class="row">


            <div class="col-md-12">

				<h3>Register your License</h3> <br />
				
				<p>To enjoy all the features of WordpressPete you need to buy the full version at: <a href="https://wordpresspete.com/subscriptions/">https://wordpresspete.com/subscriptions</a></p>
				<br />
				<form action="/validate_site" method="get">
				
				  Registered email: <input type="text" id="oemail" name="oemail" class="form-control" value ="{{$pete_options->get_meta_value('validation_user_email')}}"><br>
					
				  API Key <input type="text" id="oapi_key" name="oapi_key" class="form-control" value ="{{$pete_options->get_meta_value('validation_api_key')}}"><br>
				 
				  <input id="register_action" type="submit" class="btnpete" value="Submit">
				  
				  
				  <br />

				  
				</form>
				
            </div>
     </div>
	 
	 <script>
		
	
 		function callback(data){
 		  console.log(data);
		  console.log(data.key)
		  
		  if(data.message){
			  if(data.message == "already_exsists"){
				alert("already_exsists");
				console.log("same domain");
			  	window.location.replace("/");
			  }else{
	  			alert(data.message);
	  			$("#loadMe").modal("hide");
			  }
		  }
		  
		  if(data.api_key){
		  	
  			jQuery.ajax({
  				url: "/update_license",
  				dataType: 'JSON',
      			type: 'GET',
  				data: {validation_user_email: data.user_email, validation_api_key: data.api_key, created_at_string: data.created_at_string, token: data.token},
				success: function(data){
					window.location.replace("/");
				}
  			});
			
		  }
 		}
		 
		$("#register_action").click(function() {  
			
			
			
			jQuery.ajax({
				url: "{{$validation_url}}/validate_site_json",
				dataType: 'JSONP',
    			jsonpCallback: 'callback',
    			type: 'GET',
				data: {odomain : '{{str_replace("/register_your_license","",Request::url())}}', oapi_key: $("#oapi_key").val(), oemail: $("#oemail").val()}
			});
			
			return false;
			
		});
		
		 
	</script>
	

@endsection



