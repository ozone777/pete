Amitav\Todo\TodoServiceProvider::class,

php artisan vendor:publish --provider="Amitav\Todo\TodoServiceProvider" 

composer.phar require amitav/todo:dev-master
php artisan vendor:publish --provider="vendor\amitav\todo\src\TodoServiceProvider" --tags="config"

#Package para hacer autoload de los ServiceProvider en packages para Laravel 5.1
composer.phar require appzcoder/laravel-package-discovery:dev-master
composer.phar dump-autoload
php artisan vendor:publish

composer.phar require amitav/todo:dev-master
composer.phar remove amitav/todo

#Para WordPres Laravel Plugin instalar wordpress laravel plugin:
composer.phar require peteconsuegra/wordpress-plus-laravel-plugin:dev-master
php artisan addoption --option_name=wordpress_plus_laravel --option_value=/wordpress_plus_laravel --option_category=sidebar --option_privileges=all --option_order=2
composer.phar dump-autoload
php artisan vendor:publish

#Para Desnistalar wordpress laravel plugin:
composer.phar remove peteconsuegra/wordpress-plus-laravel-plugin
php artisan removeoption --option_value=/wordpress_plus_laravel
composer.phar dump-autoload

#Para Habilitar el desarrollo en GIT  desde el vendor folder:
ir a la carpeta /vendor/wordpress-plus-laravel-plugin/.git y editar el config de esta manera:
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
	ignorecase = true
	precomposeunicode = true
[remote "origin"]
	url = https://github.com/peterconsuegra/wordpress-plus-laravel-plugin.git
	fetch = +refs/heads/*:refs/remotes/origin/*

Para desistalar
composer.phar remove peteconsuegra/wordpress-plus-laravel-plugin

#Comandos para sidebar dynamic
php artisan addoption --option_name=wordpress_sites --option_value=/sites --option_category=sidebar --option_privileges=all --option_order=1 

php artisan addoption --option_name=wordpress_plus_laravel --option_value=/wordpress_plus_laravel --option_category=sidebar --option_privileges=all --option_order=2

php artisan addoption --option_name=plugins --option_value=/pete_plugins --option_category=sidebar --option_privileges=all --option_order=10

php artisan addoption --option_name=phpinfo --option_value=http://phpinfo.test --option_category=sidebar --option_privileges=all --option_order=11

php artisan addoption --option_name=phpmyadmin --option_value=http://phpmyinfo.test --option_category=sidebar --option_privileges=all --option_order=12

php artisan addoption --option_name=options --option_value=/options --option_category=sidebar --option_privileges=all --option_order=13

php artisan addoption --option_name=users --option_value=/list_users --option_category=sidebar --option_privileges=admin --option_order=14

