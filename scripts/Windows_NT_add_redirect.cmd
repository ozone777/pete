@echo off
setlocal enabledelayedexpansion
:ARGUMENTS_LOOP
if "%~1"==""  ( goto SETVARIABLES)
    if /I "%~1" == "-v" (set os=%~2& goto SHIFT)
    if /I "%~1" == "-n" (set adomain_name=%~2& goto SHIFT)
    if /I "%~1" == "-u" (set site_url=%~2& goto SHIFT)
    if /I "%~1" == "-r" (set route=%~2& goto SHIFT)
    if /I "%~1" == "-a" (set apache_conf=%~2& goto SHIFT)
    if /I "%~1" == "-s" (set id=%~2& goto SHIFT)
    if /I "%~1" == "-x" (set filename=%~2& goto SHIFT)
    if /I "%~1" == "-j" (set os_version=%~2& goto SHIFT)
    if /I "%~1" == "-t" (set server=%~2& goto SHIFT)
    if /I "%~1" == "-w" (set server_version=%~2& goto SHIFT)
    if /I "%~1" == "-k" (set debug=%~2& goto SHIFT)

:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:SETVARIABLES

if /I "%server%" == "apache" (
    (
    echo ^<VirtualHost *:80^>
        echo ServerName %adomain_name%
	    echo Redirect 301 / http://%site_url%
	echo ^</VirtualHost^>
    ) > "%apache_conf%/%filename%.conf"

    ::Agrega al host del sistema
    echo.>> "C:\Windows\System32\drivers\etc\hosts"
    echo 127.0.0.1 %adomain_name% >> "C:\Windows\System32\drivers\etc\hosts"
    echo ::1 %adomain_name% >> "C:\Windows\System32\drivers\etc\hosts"
)

if /I "%debug%" == "active"	( goto debug)

:DEBUG
echo os: %os%
echo adomain_name: %adomain_name%
echo site_url: %site_url%
echo route: %route%
echo apache_conf: %apache_conf%
echo id: %id%
echo filename: %filename%
echo os_version: %os_version%
echo server: %apache_version%
echo server_version: %server_version%
echo debug: %debug%
