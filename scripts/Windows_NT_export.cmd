@echo off
setlocal enabledelayedexpansion
:ARGUMENTS_LOOP
if "%~1"==""  ( goto SETVARIABLES)
    if /I "%~1" == "-n" (set project_name=%~2& goto SHIFT)
    if /I "%~1" == "-p" (set db_root_pass=%~2& goto SHIFT)
    if /I "%~1" == "-r" (set route=%~2& goto SHIFT)
    if /I "%~1" == "-m" (set mysql_bin=%~2& goto SHIFT)
    if /I "%~1" == "-v" (set os=%~2& goto SHIFT) 
    if /I "%~1" == "-w" (set server=%~2& goto SHIFT)
    if /I "%~1" == "-t" (set server_version=%~2& goto SHIFT)
    if /I "%~1" == "-u" (set site_url=%~2& goto SHIFT)  
    if /I "%~1" == "-a" (set app_name=%~2& goto SHIFT)
    if /I "%~1" == "-k" (set debug=%~2& goto SHIFT)
    if /I "%~1" == "-odb" (set odb=%~2& goto SHIFT)
    if /I "%~1" == "-pr" (set prefix=%~2& goto SHIFT)
        
:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:SETVARIABLES

::CRUD logic
set crud_route=%route%\Pete\operations\crud
rmdir /s /q %route%\Pete\operations\crud
mkdir %route%\Pete\operations\crud

(
    echo domain: '%site_url%'
    echo platform: '%app_name%'
    echo prefix: '%prefix%'
) > "%crud_route%\config.txt"

if /I "%db_root_pass%" == "none" (
	%mysql_bin% --host=localhost -uroot %odb% > %crud_route%\query.sql
) else (
	%mysql_bin% --host=localhost -uroot -p%db_root_pass% %odb% > %crud_route%\query.sql
)

rmdir /s /q %route%\Pete\public\export
mkdir %route%\Pete\public\export
mkdir %crud_route%\massive_file
xcopy %route%\%project_name% %crud_route%\massive_file\filem\* /E
move %crud_route%\query.sql %crud_route%\massive_file\query.sql
move %crud_route%\config.txt %crud_route%\massive_file\config.txt

cd %crud_route% 
C:\WordPressPete\zip\7za.exe -mx=0 a massive_file.zip massive_file
move %crud_route%\massive_file.zip %route%\Pete\public\export\%project_name%.zip

::#cd %crud_route% && zip -r massive_file.zip massive_file
::#mv %crud_route%\massive_file.zip %route%\Pete\public\export\%project_name%.zip

if /I "%debug%" == "active"	( goto debug)

:DEBUG
echo project_name: %project_name%
echo db_root_pass: %db_root_pass%
echo route: %route%
echo mysql_bin: %mysql_bin%
echo os: %os%
echo server: %server%
echo server_version: %server_version%
echo site_url: %site_url%
echo debug: %debug%

