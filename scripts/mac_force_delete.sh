#!/bin/bash

while getopts v:n:r:a:s:j:l:w:k: option 
do 
case "${option}" 
	in 
	v) os=${OPTARG};;
	n) project_name=${OPTARG};; 
	r) route=${OPTARG};; 
	a) apache_conf=${OPTARG};;
	s) id=${OPTARG};;
	j) os_version=${OPTARG};;
	l) server=${OPTARG};;
	w) server_version=${OPTARG};;
	k) debug=${OPTARG};;
esac 
done 

#DEBUG
if test "$debug" = 'active'; then
	
echo os: $os
echo project_name: $project_name
echo route: $route
echo apache_conf: $apache_conf
echo id: $id
echo os_version: $debug
echo server: $server
echo server_version: $server_version
echo debug: $debug

fi

echo "rm -rf $route/Pete/trash/$project_name-$id"
rm -rf $route/Pete/trash/$project_name-$id


