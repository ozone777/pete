@echo off
setlocal enabledelayedexpansion
:ARGUMENTS_LOOP
if "%~1"==""  ( goto SETVARIABLES)
    if /I "%~1" == "-n" (set project_name=%~2& goto SHIFT)
    if /I "%~1" == "-p" (set db_root_pass=%~2& goto SHIFT)
    if /I "%~1" == "-r" (set route=%~2& goto SHIFT)
    if /I "%~1" == "-m" (set mysql_bin=%~2& goto SHIFT)
    if /I "%~1" == "-u" (set project_url=%~2& goto SHIFT)
    if /I "%~1" == "-b" (set wpkey1=%~2& goto SHIFT)
    if /I "%~1" == "-c" (set wpkey2=%~2& goto SHIFT)
    if /I "%~1" == "-d" (set wpkey3=%~2& goto SHIFT)
    if /I "%~1" == "-e" (set wpkey4=%~2& goto SHIFT)
    if /I "%~1" == "-f" (set wpkey5=%~2& goto SHIFT)
    if /I "%~1" == "-g" (set wpkey6=%~2& goto SHIFT)
    if /I "%~1" == "-h" (set wpkey7=%~2& goto SHIFT)
    if /I "%~1" == "-i" (set wpkey8=%~2& goto SHIFT)
    if /I "%~1" == "-a" (set apache_conf=%~2& goto SHIFT)
    if /I "%~1" == "-x" (set database_name=%~2& goto SHIFT)
    if /I "%~1" == "-y" (set database_user=%~2& goto SHIFT)
    if /I "%~1" == "-l" (set password_user=%~2& goto SHIFT)
    if /I "%~1" == "-v" (set os=%~2& goto SHIFT)
    if /I "%~1" == "-j" (set os_version=%~2& goto SHIFT)
    if /I "%~1" == "-t" (set server=%~2& goto SHIFT)
    if /I "%~1" == "-w" (set server_version=%~2& goto SHIFT)
    if /I "%~1" == "-o" (set file_route=%~2& goto SHIFT)
    if /I "%~1" == "-s" (set type=%~2& goto SHIFT)
    if /I "%~1" == "-k" (set debug=%~2& goto SHIFT)

:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:SETVARIABLES
::CRUD logic
set crud_route=%route%\Pete\operations\crud
rmdir /s /q %route%\Pete\operations\crud
mkdir %route%\Pete\operations\crud

::OBTAINING DATA##############
if /I "%type%" == "url" (	
    cd %crud_route%
	curl -O -J -L %file_route%
    dir /b>hola
    set /p po=<hola
    set file_route=%crud_route%\!po!
    del hola
)       
if not x%file_route:.gz=%==x%file_route% (
    C:\WordPressPete\zip\7za.exe x "%file_route%" -so | C:\WordPressPete\zip\7za.exe x -aoa -si -ttar -o%crud_route%
) else ( 
    if not x%file_route:.zip=%==x%file_route% (
        C:\WordPressPete\zip\7za.exe x "%file_route%" -o%crud_route%
    ) else ( echo "Invalid extension" && goto debug )
)

move %crud_route%\massive_file\filem %route%\%project_name%

cd %crud_route%\massive_file
findstr /c:"domain" config.txt > hola
set /p past_url=<hola
set past_url=%past_url:~8%
set past_url=%past_url:'=%
set past_url=%past_url: =%

findstr /c:"prefix" config.txt > hola
set /p prefix=<hola
set prefix=%prefix:~8%
set prefix=%prefix:'=%
set prefix=%prefix: =%

findstr /c:"platform" config.txt > hola
set /p platform=<hola
set platform=%platform:~10%
set platform=%platform:'=%
set platform=%platform: =%
del hola

(
echo define^('WP_HOME','http://%project_url%'^);
echo define^('WP_SITEURL','http://%project_url%'^);
echo define^('DB_NAME', '%database_name%'^);
echo define^('DB_USER', '%database_user%'^);
echo define^('DB_PASSWORD', '%password_user%'^);
echo.
echo define^('AUTH_KEY',         '%wpkey1%'^);
echo define^('SECURE_AUTH_KEY',  '%wpkey2%'^);
echo define^('LOGGED_IN_KEY',    '%wpkey3%'^);
echo define^('NONCE_KEY',        '%wpkey4%'^);
echo define^('AUTH_SALT',        '%wpkey5%'^);
echo define^('SECURE_AUTH_SALT', '%wpkey6%'^);
echo define^('LOGGED_IN_SALT',   '%wpkey7%'^);
echo define^('NONCE_SALT',       '%wpkey8%'^);
echo $table_prefix  = '%prefix%';
) > "%route%\otemp.txt"

del %route%\%project_name%\wp-config.php
cd %route%
copy "%route%\Pete\templates\w1_wp-template-without-prefix.php"+"%route%\otemp.txt"+"%route%\Pete\templates\w2_wp-template-without-prefix.php" "%route%\%project_name%\wp-config.php" /b
::rm -rf otemp.txt

::Rename urls in database
cd %crud_route%\massive_file && sed -e "s/%past_url%/%project_url%/g" query.sql > query1.sql
cd %crud_route%\massive_file && sed -e "s/https/http/g" query1.sql > query_new.sql
move %crud_route%\massive_file\query_new.sql %crud_route%\query.sql

::Delete the really-simple-ssl plugin
del %route%\%project_name%\wp-content\plugins\really-simple-ssl

::Mysql Commands
if /I "%db_root_pass%" == "none" (

%mysql_bin% --host=localhost -uroot -e "create database %database_name%"
%mysql_bin% --host=localhost -uroot -e "CREATE USER %database_user%@localhost"
%mysql_bin% --host=localhost -uroot -e "SET PASSWORD FOR %database_user%@localhost = PASSWORD('%password_user%')"
%mysql_bin% --host=localhost -uroot -e "GRANT ALL PRIVILEGES ON %database_name%.* TO %database_user%@localhost IDENTIFIED BY '%password_user%'"
%mysql_bin% --host=localhost -uroot %database_name% < %crud_route%\query.sql

) else (

%mysql_bin% --host=localhost -uroot -p%db_root_pass% -e "create database %database_name%"
%mysql_bin% --host=localhost -uroot -p%db_root_pass% -e "CREATE USER %database_user%@localhost"
%mysql_bin% --host=localhost -uroot -p%db_root_pass% -e "SET PASSWORD FOR %database_user%@localhost = PASSWORD('%password_user%')"
%mysql_bin% --host=localhost -uroot -p%db_root_pass% -e "GRANT ALL PRIVILEGES ON %database_name%.* TO %database_user%@localhost IDENTIFIED BY '%password_user%'"
%mysql_bin% --host=localhost -uroot -p%db_root_pass% %database_name% < %crud_route%\query.sql

)

::APACHE OPTIONS#############
if /i "%server%" == "apache" (

	mkdir C:\WordPressPete\Apache24\logs\%project_name%
	
	(
    echo ^<VirtualHost *:80^>
    echo ServerName %project_url%
	    echo ServerAlias www.%project_url%
	    echo DocumentRoot "%route%\%project_name%"
		echo.
	      echo ^<Directory "%route%\%project_name%"^>
              echo SetOutputFilter DEFLATE
              echo Options FollowSymLinks
              echo AllowOverride All
              echo Order Deny,Allow
              echo Require all granted
	      echo ^</Directory^>
		echo.
  	    echo ErrorLog "logs\%project_name%\error.log"
  	    echo CustomLog "logs\%project_name%\requests.log" combined
	echo ^</VirtualHost^>
    ) > "%apache_conf%\%project_name%.conf"

    ::Agrega al host del sistema
    echo.>> "C:\Windows\System32\drivers\etc\hosts"
    echo 127.0.0.1 %project_name%.test >> "C:\Windows\System32\drivers\etc\hosts"
    echo ::1 %project_name%.test >> "C:\Windows\System32\drivers\etc\hosts"
	
)

if /I "%debug%" == "active"	( goto debug)

:DEBUG
echo project_name: %project_name%
echo db_root_pass: %db_root_pass%
echo route: %route%
echo mysql_bin: %mysql_bin%
echo project_url: %project_url%
echo wpkey1: %wpkey1%
echo wpkey2: %wpkey2%
echo wpkey3: %wpkey3%
echo wpkey4: %wpkey4%
echo wpkey5: %wpkey5%
echo wpkey6: %wpkey6%
echo wpkey7: %wpkey7%
echo wpkey8: %wpkey8%
echo apache_conf: %apache_conf%
echo database_name: %database_name%
echo database_user: %database_user%
echo password_user: %password_user%
echo os: %os%
echo os_version: %os_version%
echo server: %server%
echo server_version: %server_version%
echo file_route: %file_route%
echo type: %type%
echo past_url: %past_url%
echo prefix: %prefix%
echo platform: %platform%
