#!/bin/bash

while getopts r:o: option 
do 
case "${option}" 
	in 
	r) route=${OPTARG};; 
	o) os=${OPTARG};;
esac 
done 

cd $route

rm -rf latest
rm -rf latest.zip
cp -r Pete latest
rm -rf $route/latest/public/export/*
rm -rf $route/latest/.git
rm -rf $route/latest/vendor/*
rm -rf $route/latest/trash/*

if test "$os" = 'mac'; then
	zip -r latest.zip latest -x "*.DS_Store"
fi

if test "$os" = 'olinux'; then
	zip -r latest.zip latest
fi
