@echo on
:ARGUMENTS_LOOP
if "%~1"==""  ( goto SETVARIABLES)
    if /I "%~1" == "-p" (set db_root_pass=%~2& goto SHIFT)
    if /I "%~1" == "-r" (set route=%~2& goto SHIFT)
    if /I "%~1" == "-m" (set mysqlcommand=%~2& goto SHIFT)
    if /I "%~1" == "-a" (set apache_conf=%~2& goto SHIFT)
    if /I "%~1" == "-b" (set wordpress_laravel_git_branch=%~2& goto SHIFT)
    if /I "%~1" == "-g" (set wordpress_laravel_git=%~2& goto SHIFT)
    if /I "%~1" == "-u" (set wordpress_laravel_url=%~2& goto SHIFT)
    if /I "%~1" == "-j" (set os_version=%~2& goto SHIFT)
    if /I "%~1" == "-v" (set os=%~2& goto SHIFT)
    if /I "%~1" == "-n" (set project_name=%~2& goto SHIFT)
    if /I "%~1" == "-l" (set wp_load_path=%~2& goto SHIFT)
    if /I "%~1" == "-e" (set wp_url=%~2& goto SHIFT)
    if /I "%~1" == "-s" (set id=%~2& goto SHIFT)
    if /I "%~1" == "-t" (set server=%~2& goto SHIFT)
	if /I "%~1" == "-c" (action_name=%~2& goto SHIFT)
	if /I "%~1" == "-o" (laravel_version=%~2& goto SHIFT)
    if /I "%~1" == "-w" (set server_version=%~2& goto SHIFT)
    if /I "%~1" == "-k" (set debug=%~2& goto SHIFT)
    if /I "%~1" == "-odb" (set odb=%~2& goto SHIFT)
    if /I "%~1" == "-udb" (set udb=%~2& goto SHIFT)
    if /I "%~1" == "-pdb" (set pdb=%~2& goto SHIFT)

:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:SETVARIABLES

IF %action_namer% EQU "New" (
	goto NEXT0 
) else (
	goto NEXT1
)

:NEXT0
	
	echo "New action"
	echo "cd %route% && php /usr/local/bin/composer.phar create-project laravel/laravel=%laravel_version% %project_name% --prefer-dist 2>&1"
	cd %route%
	cmd /c composer create-project laravel/laravel=%laravel_version% %project_name% --prefer-dist 
	echo "cd %route%/%project_name% && php /usr/local/bin/composer.phar require peteconsuegra/wordpress-plus-laravel"
	:: cd %route%/%project_name% && php /usr/local/bin/composer.phar require peteconsuegra/wordpress-plus-laravel
	cd %route%/%project_name% 
	cmd /c composer require peteconsuegra/wordpress-plus-laravel:dev-master
	cd %route%/%project_name% 
	ren .env.example .env
	set valid = "true"
	goto NEXT3
	
:NEXT1
	echo "Import action"
	echo "cd %route% && git clone -b %wordpress_laravel_git_branch% %wordpress_laravel_git% %project_name%"
	cd %route%
	C:\WordPressPete\PGit\cmd\git clone -b %wordpress_laravel_git_branch% %wordpress_laravel_git% %project_name%
	echo "cd %route%/%project_name% && composer install"
	cd %route%/%project_name%
	cmd /c composer install
	cd %route%/%project_name%
	cmd /c composer require peteconsuegra/wordpress-plus-laravel:dev-master
	cd %route%/%project_name%
	ren .env.example .env
	goto NEXT3




:NEXT3
:: APACHE OPTIONS############# 
IF %server% EQU "apache" (
	goto NEXT
) else (
	if /I "%debug%" == "active"	( goto DEBUG)
)

 :NEXT

  (
	echo APP_ENV=local
	echo APP_DEBUG=true
	echo.
	echo DB_HOST=localhost
	echo DB_DATABASE=%odb%
	echo DB_USERNAME=%udb%
	echo DB_PASSWORD=%pdb%
	echo.
	echo CACHE_DRIVER=file
	echo SESSION_DRIVER=file
	echo QUEUE_DRIVER=sync
	echo.
	echo MAIL_DRIVER=smtp
	echo MAIL_HOST=mailtrap.io
	echo MAIL_PORT=2525
	echo MAIL_USERNAME=null
	echo MAIL_PASSWORD=null
	echo MAIL_ENCRYPTION=null
	echo.
	echo WP_LOAD_PATH=%wp_load_path%
	echo WP_URL=http://%wp_url%
	echo.
  ) > %route%/%project_name%/.env

	%mysql_bin% --host=localhost -uroot -p%db_root_pass% -e "create database %odb%"
	
	mkdir C:\WordPressPete\Apache24\logs\%project_name%

	(
    echo ^<VirtualHost *:80^>
    echo ServerName %wordpress_laravel_url%
	    echo ServerAlias www.%wordpress_laravel_url%
	    echo DocumentRoot "%route%\%project_name%\public"
  	    echo ErrorLog "logs\%project_name%\error.log"
  	    echo CustomLog "logs\%project_name%\requests.log" combined
	echo ^</VirtualHost^>
    ) > "%apache_conf%\%project_name%.conf"
	:: $server_conf/$project_name.conf Original

	::Agrega al host del sistema
    echo.>> "C:\Windows\System32\drivers\etc\hosts"
    echo 127.0.0.1 %wordpress_laravel_url% >> "C:\Windows\System32\drivers\etc\hosts"
    echo ::1 %wordpress_laravel_url% >> "C:\Windows\System32\drivers\etc\hosts"

	echo "Running the following commands..."
	echo "cd %route%/%project_name% && php artisan key:generate"
	echo "cd %route%/%project_name% && php artisan migrate"
 
	::NOT SURE 2>&1 ?
	cd %route%\%project_name%
	::First we need to erase APP_KEY= form .env file
	set key_info="APP_KEY="
	set key_replace=""
	cd %route%/%project_name% 
	sed -i '' "s/%key_info%/%key_replace%/g" .env
	php artisan key:generate --show > holi
	set /p po=<holi
	echo "laravel key: %po%"
	echo APP_KEY=%po% >> .env 
	del holi	
	cmd /c php artisan migrate

	echo ""
	echo "#########################################"
	echo "Executing aditional operations"
	cd %route%/%project_name% 
	cmd /c php artisan new_wordpress_plus_laravel



	
if /I "%debug%" == "active"	( goto DEBUG)

:DEBUG
echo db_root_pass: %db_root_pass%
echo app_root: %route %
echo mysqlcommand: %mysqlcommand%
echo apache_conf: %apache_conf%
echo wordpress_laravel_git_branch: %wordpress_laravel_git_branch%
echo wordpress_laravel_git: %wordpress_laravel_git%
echo wordpress_laravel_url: %wordpress_laravel_url%
echo os_version: %os_version%
echo os: %os%
echo site_name: %project_name%
echo wp_load_path: %wp_load_path%
echo wp_url: %wp_url%
echo id %id%
echo apache_version %apache_version%
echo debug %debug%
echo DB: %odb%
echo UDB: %udb%
echo PDB: %pdb%