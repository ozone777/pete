@echo off
setlocal enabledelayedexpansion
:ARGUMENTS_LOOP
if "%~1"==""  ( goto SETVARIABLES)
    if /I "%~1" == "-v" (set server=%~2& goto SHIFT)
    if /I "%~1" == "-n" (set project_name=%~2& goto SHIFT)
    if /I "%~1" == "-p" (set db_root_pass=%~2& goto SHIFT)
    if /I "%~1" == "-r" (set route=%~2& goto SHIFT)
    if /I "%~1" == "-m" (set mysql_bin=%~2& goto SHIFT)
    if /I "%~1" == "-a" (set apache_conf=%~2& goto SHIFT)
    if /I "%~1" == "-w" (set server_version=%~2& goto SHIFT)
    if /I "%~1" == "-s" (set id=%~2& goto SHIFT)    
    if /I "%~1" == "-u" (set project_url=%~2& goto SHIFT)   
    if /I "%~1" == "-o" (set os=%~2& goto SHIFT) 
    if /I "%~1" == "-j" (set os_version=%~2& goto SHIFT)    
    if /I "%~1" == "-k" (set debug=%~2& goto SHIFT)
        
:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:SETVARIABLES

cd %route%

if /I "%server%" == "apache"  (
::needs improvement
    (
    echo ^<VirtualHost *:80^>
    echo ServerName %project_url%
	    echo ServerAlias www.%project_url%
	    echo DocumentRoot "%route%\%project_name%"
		echo.
	      echo ^<Directory "%route%\%project_name%"^>              
              echo SetOutputFilter DEFLATE
              echo Options FollowSymLinks
              echo AllowOverride All
              echo Order Deny,Allow
              echo Require all granted
	      echo ^</Directory^>
		echo.
        echo ErrorLog "logs\%project_name%\error.log"
  	    echo CustomLog "logs\%project_name%\requests.log" combined
	echo ^</VirtualHost^>
    ) > "%apache_conf%\%project_name%.conf"

    ::Agrega al host del sistema
    echo.>> "C:\Windows\System32\drivers\etc\hosts"
    echo 127.0.0.1 %project_name%.test >> "C:\Windows\System32\drivers\etc\hosts"
    echo ::1 %project_name%.test >> "C:\Windows\System32\drivers\etc\hosts"

    move %route%/Pete/trash/%project_name%-%id% %route%/%project_name%

)

if /I "%debug%" == "active"	( goto debug)

:DEBUG
    echo server: %server%
    echo server_version: %server_version%
    echo project_name: $project_name%
    echo db_root_pass: $db_root_pass %
    echo route: $route%
    echo mysql_bin: %mysql_bin%
    echo apache_conf: %apache_conf%
    echo app_name: %app_name%
    echo id: $id%
    echo project_url: %project_url%
    echo os_version: %os_version%
    echo debug: %debug%
    echo os: %os%

