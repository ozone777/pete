@echo off
setlocal enabledelayedexpansion
:ARGUMENTS_LOOP
if "%~1"=="" ( goto SETVARIABLES)
if /I "%~1" == "-v" (set os=%~2& goto SHIFT)
if /I "%~1" == "-n" (set project_name=%~2& goto SHIFT)
if /I "%~1" == "-r" (set route=%~2& goto SHIFT)
if /I "%~1" == "-a" (set apache_conf=%~2& goto SHIFT)
if /I "%~1" == "-s" (set id=%~2& goto SHIFT)
if /I "%~1" == "-j" (set os_version=%~2& goto SHIFT)
if /I "%~1" == "-l" (set server=%~2& goto SHIFT)
if /I "%~1" == "-w" (set server_version=%~2& goto SHIFT)
if /I "%~1" == "-k" (set debug=%~2& goto SHIFT)
if /I "%~1" == "-url" (set url=%~2& goto SHIFT)
:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:SETVARIABLES

if /I "%server%" == "apache" (
del %apache_conf%\%project_name%.conf

cd "C:\Windows\System32\drivers\etc"
awk "{if($2!~/^%url%$/) {} else {print $0;}}" hosts > hosts.txt && del hosts && ren hosts.txt hosts
)
move %route%/%project_name% %route%/Pete/trash/%project_name%-%id%

if /I "%debug%" == "active" ( goto debug)

:DEBUG
echo os: %os%
echo project_name: %project_name%
echo route: %route%
echo apache_conf: %apache_conf%
echo id: %app_name%
echo os_version: %debug%
echo server: %server%
echo server: %server_version%
echo debug: %debug%
