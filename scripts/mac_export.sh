#!/bin/bash

while getopts n:p:r:m:v:w:t:u:a:q:i:k: option 
do 
case "${option}" 
	in 
	n) project_name=${OPTARG};; 
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	m) mysql_bin=${OPTARG};;
	v) os=${OPTARG};;
	w) server=${OPTARG};;
	t) server_version=${OPTARG};;
	u) site_url=${OPTARG};;
	a) app_name=${OPTARG};;
	q) odb=${OPTARG};;
	i) prefix=${OPTARG};;
	k) debug=${OPTARG};;
esac 
done 

if test "$debug" = 'active'; then
	
echo project_name: $project_name
echo db_root_pass: $db_root_pass
echo route: $route
echo mysql_bin: $mysql_bin
echo os: $os
echo server: $server
echo server_version: $server_version
echo site_url: $site_url
echo debug: $debug
	
fi

#CRUD logic
crud_route=$route/Pete/operations/crud
rm -rf $route/Pete/operations/crud/*

echo "domain: '$site_url'
platform: '$app_name'
prefix: '$prefix'
" > $crud_route/config.txt

if test "$db_root_pass" = 'none'; then
	$mysql_bin --host=localhost -uroot $odb > $crud_route/query.sql
else
	$mysql_bin --host=localhost -uroot -p$db_root_pass $odb > $crud_route/query.sql
fi

rm -rf $route/Pete/public/export/*
mkdir $crud_route/massive_file
cp -r $route/$project_name $crud_route/massive_file/filem
mv $crud_route/query.sql $crud_route/massive_file/query.sql
mv $crud_route/config.txt $crud_route/massive_file/config.txt
chmod -R 755 $crud_route/massive_file

cd $crud_route && tar czf massive_file.tar.gz massive_file
mv $crud_route/massive_file.tar.gz $route/Pete/public/export/$project_name.tar.gz

#cd $crud_route && zip -r massive_file.zip massive_file
#mv $crud_route/massive_file.zip $route/Pete/public/export/$project_name.zip

rm -rf $route/Pete/operations/crud/*


