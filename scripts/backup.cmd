@echo off
setlocal enabledelayedexpansion
:ARGUMENTS_LOOP
if "%~1"==""  ( goto SETVARIABLES)
    if /I "%~1" == "-n" (set project_name=%~2& goto SHIFT)
    if /I "%~1" == "-p" (set db_root_pass=%~2& goto SHIFT)
    if /I "%~1" == "-r" (set route=%~2& goto SHIFT)
    if /I "%~1" == "-m" (set mysql_bin=%~2& goto SHIFT)
    if /I "%~1" == "-v" (set os=%~2& goto SHIFT)
    if /I "%~1" == "-w" (set app_name=%~2& goto SHIFT)
    if /I "%~1" == "-u" (set site_url=%~2& goto SHIFT)
    if /I "%~1" == "-k" (set debug=%~2& goto SHIFT)    
    if /I "%~1" == "-f" (set backup_url=%~2& goto SHIFT)    

:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:SETVARIABLES

if /I "%debug%" == "active"	( goto debug)
goto next

:DEBUG
    echo project_name: %project_name%
    echo db_root_pass: %db_root_pass%
    echo route: %route%
    echo mysql_bin: %mysql_bin%
    echo os: %os%
    echo app_name: %app_name%
    echo site_url: %site_url%
    echo debug: %debug%
    echo debug: %backup_url%
goto next

:NEXT
rmdir /q /s %route%/backups_playground/massive_file
mkdir %route%/backups_playground/massive_file

::CRUD logic
set backups_route=%route%/Pete/operations/backups
::CLEAN OPERATIONS FOLDER
rmdir /q /s %route%/Pete/operations/backups
mkdir %route%/Pete/operations/backups

mkdir %backups_route%/massive_file

if /I "%app_name%" == "Wordpress" (
cd %route%
findstr /c:"define('DB_NAME'," %project_name%/wp-config.php > hola
set /p odb=<hola
set odb=%odb:~19,13%

::Get DB Prefix
findstr /c:"table_prefix" %project_name%/wp-config.php > hola
set /p prefix=<hola  
set prefix="${prefix// /}"
set prefix=${prefix#*="'"}
set prefix=${prefix%"'";}
)

echo "domain: '%site_url%'
platform: '%app_name%'
prefix: '$prefix'
" > %backups_route%/massive_file/config.txt

if test "%db_root_pass%" = 'none'; then
	%mysql_bin% --host=localhost -uroot $odb > %backups_route%/massive_file/query.sql
else
	%mysql_bin% --host=localhost -uroot -p%db_root_pass% $odb > %backups_route%/massive_file/query.sql
fi

cp -r %route%/%project_name% %backups_route%/massive_file/filem
chmod -R 755 %backups_route%/massive_file
cd %backups_route% && tar czf massive_file.tar.gz massive_file
mv %backups_route%/massive_file.tar.gz %route%/Pete/backups/%backup_url%


#CLEAN OPERATIONS FOLDER
rm -rf %route%/Pete/operations/backups/*

