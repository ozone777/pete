#!/bin/bash

while getopts v:n:u:r:a:s:x:j:t:k: option 
do 
case "${option}" 
	in 
	v) os=${OPTARG};;
	n) adomain_name=${OPTARG};; 
	u) site_url=${OPTARG};; 
	r) route=${OPTARG};; 
	a) apache_conf=${OPTARG};;
	s) id=${OPTARG};;
	x) filename=${OPTARG};;
	j) os_version=${OPTARG};;
	t) server=${OPTARG};;
	w) server_version=${OPTARG};;
	k) debug=${OPTARG};;
esac 
done 

#DEBUG
if test "$debug" = 'active'; then
	
echo os: $os
echo adomain_name: $adomain_name
echo site_url: $site_url
echo route: $route
echo apache_conf: $apache_conf
echo id: $id
echo filename: $filename
echo os_version: $os_version
echo server: $server
echo server_version: $server_version
echo debug: $debug

fi


if test "$server" = 'apache'; then
	echo "
	<VirtualHost *:80>
	        ServerName $adomain_name
	        Redirect 301 / http://$site_url
	</VirtualHost>
	" > $apache_conf/$filename.conf
fi

