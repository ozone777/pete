#!/bin/bash

while getopts o:n:q:u:p:r:a:x:y:z:b:c:d:e:f:g:h:i:v:l:j:t:w:s:m:k: option 
do 
case "${option}" 
	in 
	q) mysqldump=${OPTARG};;
	o) to_clone_project=${OPTARG};;
	n) project_name=${OPTARG};; 
	u) project_url=${OPTARG};; 
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	a) apache_conf=${OPTARG};;
	x) database_name=${OPTARG};;
	y) database_user=${OPTARG};;
	z) password_user=${OPTARG};;
	b) wpkey1=${OPTARG};;
	c) wpkey2=${OPTARG};;
	d) wpkey3=${OPTARG};;
	e) wpkey4=${OPTARG};;
	f) wpkey5=${OPTARG};;
	g) wpkey6=${OPTARG};;
	h) wpkey7=${OPTARG};;
	i) wpkey8=${OPTARG};;
	v) os=${OPTARG};;
	l) past_url=${OPTARG};;
	j) os_version=${OPTARG};;
	t) server=${OPTARG};;
	w) server_version=${OPTARG};;
	s) odb=${OPTARG};;
	m) prefix=${OPTARG};;
	k) debug=${OPTARG};;
esac 
done 

if test "$debug" = 'active'; then

echo mysqldump: $mysqldump 
echo to_clone_project: $to_clone_project
echo project_name: $project_name
echo project_url: $project_url
echo db_root_pass: $db_root_pass
echo route: $route
echo apache_conf: $apache_conf
echo database_name: $database_name
echo database_user: $database_user
echo password_user: $password_user
echo wpkey1: $wpkey1
echo wpkey2: $wpkey2
echo wpkey3: $wpkey3
echo wpkey4: $wpkey4
echo wpkey5: $wpkey5
echo wpkey6: $wpkey6
echo wpkey7: $wpkey7
echo wpkey8: $wpkey8
echo os: $os
echo past_url: $past_url
echo os_version: $os_version
echo server: $server
echo server_version: $server_version
echo odb: $odb
echo prefix: $prefix
echo debug: $debug
	
fi

#CRUD logic
crud_route=$route/Pete/operations/crud
rm -rf $route/Pete/operations/crud/*

cp -r $route/$to_clone_project $route/$project_name

echo "
define('WP_HOME','http://$project_url');
define('WP_SITEURL','http://$project_url');

define('DB_NAME', '$database_name');
define('DB_USER', '$database_user');
define('DB_PASSWORD', '$password_user');


define('AUTH_KEY',         '$wpkey1');
define('SECURE_AUTH_KEY',  '$wpkey2');
define('LOGGED_IN_KEY',    '$wpkey3');
define('NONCE_KEY',        '$wpkey4');
define('AUTH_SALT',        '$wpkey5');
define('SECURE_AUTH_SALT', '$wpkey6');
define('LOGGED_IN_SALT',   '$wpkey7');
define('NONCE_SALT',       '$wpkey8');

\$table_prefix  = '$prefix';

 " > $route/otemp.txt

rm -rf $route/$project_name/wp-config.php 
cd $route && sed "21r otemp.txt" < $route/Pete/templates/wp-template-without-prefix.php > $project_name/wp-config.php 

#Export sql
rm -rf $route/query.sql
if test "$db_root_pass" = 'none'; then
	$mysqldump --host=localhost -uroot $odb > $crud_route/query.sql
else
	$mysqldump --host=localhost -uroot -p$db_root_pass $odb > $crud_route/query.sql
fi

#Security files permissions
find $route/$project_name -type d -exec chmod 755 {} +
find $route/$project_name -type f -exec chmod 644 {} +
chmod 600 $route/$project_name/wp-config.php

#Mysql Commands
mysql_bin="mysql"

if test "$db_root_pass" = 'none'; then

$mysql_bin --host=localhost -uroot -e "create database $database_name"
$mysql_bin --host=localhost -uroot -e "CREATE USER $database_user@localhost"
$mysql_bin --host=localhost -uroot -e "SET PASSWORD FOR $database_user@localhost = PASSWORD('$password_user')"
$mysql_bin --host=localhost -uroot -e "GRANT ALL PRIVILEGES ON $database_name.* TO $database_user@localhost IDENTIFIED BY '$password_user'"
$mysql_bin --host=localhost -uroot $database_name < $crud_route/query.sql

else

$mysql_bin --host=localhost -uroot -p$db_root_pass -e "create database $database_name"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "CREATE USER $database_user@localhost"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "SET PASSWORD FOR $database_user@localhost = PASSWORD('$password_user')"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "GRANT ALL PRIVILEGES ON $database_name.* TO $database_user@localhost IDENTIFIED BY '$password_user'"
$mysql_bin --host=localhost -uroot -p$db_root_pass $database_name < $crud_route/query.sql

fi

#Rename urls in database via wp-cli
cd $route/$project_name && wp search-replace '$past_url' '$project_url'

#APACHE OPTIONS#############
if test "$server" = 'apache'; then

username=`id -un`
logs_route=/Users/$username/wwwlog
mkdir $logs_route/$project_name
touch $logs_route/$project_name/error.log
touch $logs_route/$project_name/access.log

	echo "
	<VirtualHost *:80>

	    ServerName $project_url
	    ServerAlias www.$project_url
	    DocumentRoot $route/$project_name
			
	      <Directory $route/$project_name>
              SetOutputFilter DEFLATE
              Options FollowSymLinks
              AllowOverride All
              Order Deny,Allow
              Require all granted
	      </Directory>
		
	    ErrorLog $logs_route/$project_name/error.log
	    CustomLog $logs_route/$project_name/access.log combined
		
	</VirtualHost>" > $apache_conf/$project_name.conf	

#Security .htaccess best practices
#rm -rf $route/$project_name/.htaccess
#cp $route/Pete/templates/htaccess_main.txt $route/$project_name/.htaccess
#rm -rf $route/$project_name/wp-content/uploads/.htaccess
#cp $route/Pete/templates/htaccess_content.txt $route/$project_name/wp-content/uploads/.htaccess
	
fi

rm -rf $route/Pete/operations/crud/*



