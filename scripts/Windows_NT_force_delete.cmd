@echo off
setlocal enabledelayedexpansion
:ARGUMENTS_LOOP
if "%~1"==""  ( goto SETVARIABLES)
    if /I "%~1" == "-v" (set os=%~2& goto SHIFT)
    if /I "%~1" == "-n" (set project_name=%~2& goto SHIFT)
    if /I "%~1" == "-r" (set route=%~2& goto SHIFT)
    if /I "%~1" == "-a" (set apache_conf=%~2& goto SHIFT)
    if /I "%~1" == "-s" (set id=%~2& goto SHIFT)
    if /I "%~1" == "-j" (set os_version=%~2& goto SHIFT)
    if /I "%~1" == "-l" (set server=%~2& goto SHIFT)
    if /I "%~1" == "-w" (set server_version=%~2& goto SHIFT)
    if /I "%~1" == "-k" (set debug=%~2& goto SHIFT)    

:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:SETVARIABLES
echo "rmdir /s /q %route%\Pete\trash\%project_name%-%id%"
rmdir /s /q "%route%\Pete\trash\%project_name%-%id%"

if /I "%debug%" == "active"	( goto debug)

:DEBUG
    echo os: %os%
    echo project_name: %project_name%
    echo route: %route%
    echo apache_conf: %apache_conf%
    echo id: %id%
    echo os_version: %debug%
    echo server: %server%
    echo server_version: %server_version%
    echo debug: %debug%