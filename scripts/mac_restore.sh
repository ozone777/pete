#!/bin/bash

while getopts v:n:p:r:m:a:w:s:u:j:k: option 
do 
case "${option}" 
	in 
	v) server=${OPTARG};;
	w) server_version=${OPTARG};;
	n) project_name=${OPTARG};; 
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	m) mysql_bin=${OPTARG};;
	a) apache_conf=${OPTARG};;
	s) id=${OPTARG};;
	u) project_url=${OPTARG};;
	j) os_version=${OPTARG};;
	k) debug=${OPTARG};;
	o) os=${OPTARG};;
esac 
done 

if test "$debug" = 'active'; then

echo server: $server
echo server_version: $server_version
echo project_name: $project_name
echo db_root_pass: $db_root_pass 
echo route: $route
echo mysql_bin: $mysql_bin
echo apache_conf: $apache_conf
echo app_name: $app_name
echo id: $id
echo project_url: $project_url
echo os_version: $os_version
echo debug: $debug
echo os: $os

fi

cd $route

if test "$server" = 'apache'; then

	username=`id -un`
	logs_route=/Users/$username/wwwlog

		echo "
		<VirtualHost *:80>

		    ServerName $project_url
		    ServerAlias www.$project_url
		    DocumentRoot $route/$project_name
			
		      <Directory $route/$project_name>
	              SetOutputFilter DEFLATE
	              Options FollowSymLinks
	              AllowOverride All
	              Order Deny,Allow
	              Require all granted
		      </Directory>
		
		    ErrorLog $logs_route/$project_name/error.log
		    CustomLog $logs_route/$project_name/access.log combined
		
		</VirtualHost>" > $apache_conf/$project_name.conf	

mv $route/Pete/trash/$project_name-$id $route/$project_name
	
fi



