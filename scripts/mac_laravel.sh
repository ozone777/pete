#!/bin/bash

while getopts p:r:m:a:b:g:j:v:n:t:w:c:o:k:q:y:i:u: option 
do 
case "${option}" 
	in 
	p) db_root_pass=${OPTARG};;
	r) route=${OPTARG};;
	m) mysql_bin=${OPTARG};;
	a) server_conf=${OPTARG};;
	b) wordpress_laravel_git_branch=${OPTARG};;
	g) wordpress_laravel_git=${OPTARG};;
	j) os_version=${OPTARG};;
	v) os=${OPTARG};;
	n) project_name=${OPTARG};;
	t) server=${OPTARG};;
	w) server_version=${OPTARG};;
	c) action_name=${OPTARG};;
	o) laravel_version=${OPTARG};;
	k) debug=${OPTARG};;
	q) database_name=${OPTARG};;
	y) database_user=${OPTARG};;
	i) password_user=${OPTARG};;
	u) url=${OPTARG};;
esac 
done

#DEBUG
if test "$debug" = 'active'; then

echo "db_root_pass: $db_root_pass"
echo "route: $route" 
echo "mysql_bin: $mysql_bin"
echo "server_conf: $server_conf"
echo "wordpress_laravel_git_branch: $wordpress_laravel_git_branch"
echo "wordpress_laravel_git: $wordpress_laravel_git"
echo "os_version: $os_version"
echo "os: $os"
echo "project_name: $project_name"
echo "server: $server"
echo "server_version: $server_version"
echo "action_name: $action_name"
echo "laravel_version $laravel_version"
echo "url $url"
echo "debug $debug"
echo "odb: "$database_name
echo "udb: "$database_user
echo "pdb: "$password_user

fi

if test "$action_name" = 'New'; then
	cd $route && php /usr/local/bin/composer.phar create-project laravel/laravel=$laravel_version $project_name --prefer-dist 2>&1
else
	cd $route && git clone -b $wordpress_laravel_git_branch $wordpress_laravel_git $project_name
fi

#Mysql Commands
if test "$db_root_pass" = 'none'; then

$mysql_bin --host=localhost -uroot -e "create database $database_name"
$mysql_bin --host=localhost -uroot -e "CREATE USER $database_user@localhost"
$mysql_bin --host=localhost -uroot -e "SET PASSWORD FOR $database_user@localhost = PASSWORD('$password_user')"
$mysql_bin --host=localhost -uroot -e "GRANT ALL PRIVILEGES ON $database_name.* TO $database_user@localhost IDENTIFIED BY '$password_user'"

else

$mysql_bin --host=localhost -uroot -p$db_root_pass -e "create database $database_name"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "CREATE USER $database_user@localhost"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "SET PASSWORD FOR $database_user@localhost = PASSWORD('$password_user')"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "GRANT ALL PRIVILEGES ON $database_name.* TO $database_user@localhost IDENTIFIED BY '$password_user'"

fi

rm -rf $route/$project_name/.env
	
echo "
APP_ENV=local
APP_DEBUG=true

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

DB_HOST=localhost
DB_DATABASE=$database_name
DB_USERNAME=$database_user
DB_PASSWORD=$password_user

" > $route/$project_name/.env

#APACHE OPTIONS#############
############################

if test "$server" = 'apache'; then

	username=`id -un`
    logs_route=/Users/$username/wwwlog
	mkdir $logs_route/$project_name
	touch $logs_route/$project_name/error.log
	touch $logs_route/$project_name/access.log
	
	echo "
	<VirtualHost *:80>

	    ServerName $url
	    ServerAlias www.$url
	    DocumentRoot $route/$project_name/public
			
	      <Directory $route/$project_name>
              SetOutputFilter DEFLATE
              Options FollowSymLinks
              AllowOverride All
              Order Deny,Allow
              Require all granted
	      </Directory>
		
  	    ErrorLog $logs_route/$project_name/error.log
  	    CustomLog $logs_route/$project_name/access.log combined
		
	</VirtualHost>" > $server_conf/$project_name.conf
	
find $route/$project_name -type f -exec chmod 644 {} \;    
find $route/$project_name -type d -exec chmod 755 {} \;
chmod -R ug+rwx storage $route/$project_name/bootstrap/cache

fi

echo "Running the following commands..."
echo "cd $route/$project_name && php artisan key:generate"
echo "cd $route/$project_name && php artisan migrate"

#RUN COMPOSER INSTALL
echo "cd $route/$project_name && php /usr/local/bin/composer.phar install 2>&1"
cd $route/$project_name && php /usr/local/bin/composer.phar install 2>&1
#HACK TO GENERATE APP_KEY
echo "`cd $route/$project_name && php artisan key:generate --show`"
laravel_key=`cd $route/$project_name && php artisan key:generate --show`
echo "APP_KEY=$laravel_key" >> $route/$project_name/.env

cd $route/$project_name && php artisan migrate








