@echo off
:ARGUMENTS_LOOP
if "%~1"==""  ( goto loop)
    if /I "%~1" == "-n" (set name=%~2& goto SHIFT)

:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:loop
start /wait TASKKILL /F /IM php-cgi.exe
start /wait TASKKILL /F /IM httpd.exe
start /wait net start %name%
exit