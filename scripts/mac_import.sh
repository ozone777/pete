#!/bin/bash

while getopts n:p:r:m:f:u:b:c:d:e:f:g:h:i:a:v:w:x:y:l:t:j:t:o:s:k: option 
do 
case "${option}" 
	in
	n) project_name=${OPTARG};;
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	m) mysql_bin=${OPTARG};;
	u) project_url=${OPTARG};; 
	b) wpkey1=${OPTARG};;
	c) wpkey2=${OPTARG};;
	d) wpkey3=${OPTARG};;
	e) wpkey4=${OPTARG};;
	f) wpkey5=${OPTARG};;
	g) wpkey6=${OPTARG};;
	h) wpkey7=${OPTARG};;
	i) wpkey8=${OPTARG};;
	a) apache_conf=${OPTARG};;
	x) database_name=${OPTARG};;
	y) database_user=${OPTARG};;
	l) password_user=${OPTARG};;
	v) os=${OPTARG};;
	j) os_version=${OPTARG};;
	t) server=${OPTARG};;
	w) server_version=${OPTARG};;
	o) file_route=${OPTARG};;
	s) type=${OPTARG};;
    k) debug=${OPTARG};;
esac 
done 

if test "$debug" = 'active'; then
	
echo project_name: $project_name
echo db_root_pass: $db_root_pass
echo route: $route
echo mysql_bin: $mysql_bin
echo project_url: $project_url
echo wpkey1: $wpkey1
echo wpkey2: $wpkey2
echo wpkey3: $wpkey3
echo wpkey4: $wpkey4
echo wpkey5: $wpkey5
echo wpkey6: $wpkey6
echo wpkey7: $wpkey7
echo wpkey8: $wpkey8
echo apache_conf: $apache_conf
echo database_name: $database_name
echo database_user: $database_user
echo password_user: $password_user
echo os: $os
echo os_version: $os_version
echo server: $server
echo server_version: $server_version
echo file_route: $file_route
echo type: $type
fi

#CRUD logic
crud_route=$route/Pete/operations/crud
rm -rf $route/Pete/operations/crud/*

#OBTAINING DATA##############
#############################

if test "$type" = 'none'; then
	
	if [[ $file_route = *".gz"* ]]; then
  	  cd $crud_route && gunzip -c $file_route | tar xopf -
	elif [[ $file_route = *".zip"* ]]; then
  	  cd $crud_route && unzip -a $file_route
	else
  	  echo "Invalid extension"
	fi

elif test "$type" = 'url'; then
	echo "$crud_route && curl -L $file_route | tar xopf -"
	cd $crud_route && curl -L $file_route | tar xopf -

elif test "$type" = 'file'; then
	cd $crud_route && gunzip -c $file_route | tar xopf -
fi

mv $crud_route/massive_file/filem $route/$project_name

past_url=`cd $crud_route/massive_file && grep "^domain" config.txt`
past_url="${past_url// /}"
past_url=${past_url#*"'"}
past_url=${past_url%"'"}

prefix=`cd $crud_route/massive_file && grep "^prefix" config.txt`
prefix="${prefix// /}"
prefix=${prefix#*"'"}
prefix=${prefix%"'"}

platform=`cd $crud_route/massive_file && grep "^platform" config.txt`
platform="${platform// /}"
platform=${platform#*"'"}
platform=${platform%"'"}

if test "$debug" = 'active'; then
	echo past_url: $past_url
	echo prefix: $prefix
	echo platform: $platform
fi

echo "
define('WP_HOME','http://$project_url');
define('WP_SITEURL','http://$project_url');

define('DB_NAME', '$database_name');
define('DB_USER', '$database_user');
define('DB_PASSWORD', '$password_user');


define('AUTH_KEY',         '$wpkey1');
define('SECURE_AUTH_KEY',  '$wpkey2');
define('LOGGED_IN_KEY',    '$wpkey3');
define('NONCE_KEY',        '$wpkey4');
define('AUTH_SALT',        '$wpkey5');
define('SECURE_AUTH_SALT', '$wpkey6');
define('LOGGED_IN_SALT',   '$wpkey7');
define('NONCE_SALT',       '$wpkey8');
\$table_prefix  = '$prefix';

 " > $route/otemp.txt

rm -rf $route/$project_name/wp-config.php 
cd $route && sed "21r otemp.txt" < $route/Pete/templates/wp-template-without-prefix.php > $project_name/wp-config.php 
#rm -rf otemp.txt

#Rename urls in database
mv $crud_route/massive_file/query.sql $crud_route/query.sql

#Security files permissions
find $route/$project_name -type d -exec chmod 755 {} +
find $route/$project_name -type f -exec chmod 644 {} +
chmod 600 $route/$project_name/wp-config.php

#Delete the really-simple-ssl plugin
rm -rf $route/$project_name/wp-content/plugins/really-simple-ssl

#Mysql Commands
if test "$db_root_pass" = 'none'; then

$mysql_bin --host=localhost -uroot -e "create database $database_name"
$mysql_bin --host=localhost -uroot -e "CREATE USER $database_user@localhost"
$mysql_bin --host=localhost -uroot -e "SET PASSWORD FOR $database_user@localhost = PASSWORD('$password_user')"
$mysql_bin --host=localhost -uroot -e "GRANT ALL PRIVILEGES ON $database_name.* TO $database_user@localhost IDENTIFIED BY '$password_user'"
$mysql_bin --host=localhost -uroot $database_name < $crud_route/query.sql

else

$mysql_bin --host=localhost -uroot -p$db_root_pass -e "create database $database_name"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "CREATE USER $database_user@localhost"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "SET PASSWORD FOR $database_user@localhost = PASSWORD('$password_user')"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "GRANT ALL PRIVILEGES ON $database_name.* TO $database_user@localhost IDENTIFIED BY '$password_user'"
$mysql_bin --host=localhost -uroot -p$db_root_pass $database_name < $crud_route/query.sql

fi

#Rename urls in database via wp-cli
cd $route/$project_name && wp search-replace '$past_url' '$project_url'

#APACHE OPTIONS#############
if test "$server" = 'apache'; then

username=`id -un`
logs_route=/Users/$username/wwwlog
mkdir $logs_route/$project_name
touch $logs_route/$project_name/error.log
touch $logs_route/$project_name/access.log

	echo "
	<VirtualHost *:80>

	    ServerName $project_url
	    ServerAlias www.$project_url
	    DocumentRoot $route/$project_name
			
	      <Directory $route/$project_name>
              SetOutputFilter DEFLATE
              Options FollowSymLinks
              AllowOverride All
              Order Deny,Allow
              Require all granted
	      </Directory>
		
	    ErrorLog $logs_route/$project_name/error.log
	    CustomLog $logs_route/$project_name/access.log combined
		
	</VirtualHost>" > $apache_conf/$project_name.conf	
	
	
	rm -rf $route/$project_name/.htaccess
	
	echo "
	
	# BEGIN WordPress
	<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteBase /
	RewriteRule ^index\.php$ - [L]
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule . /index.php [L]
	</IfModule>

	# END WordPress
	
	" > $route/$project_name/.htaccess
	
fi

cd $route/Pete/public/uploads && rm -rf *
cd $route/Pete/operations/crud && rm -rf *