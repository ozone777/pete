#!/bin/bash

while getopts n:r:a:v:w:s:u:o:j:k: option 
do 
case "${option}" 
	in 
	n) project_name=${OPTARG};; 
	r) route=${OPTARG};; 
	a) apache_conf=${OPTARG};;
	v) server=${OPTARG};;
	w) server_version=${OPTARG};;
	u) project_url=${OPTARG};; 
	o) os=${OPTARG};; 
	j) os_version=${OPTARG};; 
	k) debug=${OPTARG};;	
esac 
done 
 
#DEBUG
if test "$debug" = 'active'; then

echo project_name: $project_name
echo route: $route
echo apache_conf: $apache_conf
echo server: $server
echo server_version: $server_version
echo project_url: $project_url
echo os: $os
echo os_version: $os_version
echo debug: $debug

fi

cd $route

if test "$server" = 'apache'; then

rm -rf $apache_conf/$project_name.conf

echo "
<VirtualHost *:80>

    ServerName $project_url
    ServerAlias www.$project_url
    DocumentRoot $route/Pete/public/suspend
	
    <Directory $route/Pete/public>
        Require all granted
    </Directory>

</VirtualHost>" > $apache_conf/$project_name.conf

fi




