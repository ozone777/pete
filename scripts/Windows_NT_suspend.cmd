@echo off
setlocal enabledelayedexpansion
:ARGUMENTS_LOOP
if "%~1"==""  ( goto SETVARIABLES)
    if /I "%~1" == "-n" (set project_name=%~2& goto SHIFT)
    if /I "%~1" == "-r" (set route=%~2& goto SHIFT)
    if /I "%~1" == "-a" (set apache_conf=%~2& goto SHIFT)
    if /I "%~1" == "-v" (set server=%~2& goto SHIFT)
    if /I "%~1" == "-w" (set server_version=%~2& goto SHIFT)
    if /I "%~1" == "-u" (set project_url=%~2& goto SHIFT)
    if /I "%~1" == "-o" (set os=%~2& goto SHIFT)
    if /I "%~1" == "-j" (set os_version=%~2& goto SHIFT)    
    if /I "%~1" == "-k" (set debug=%~2& goto SHIFT)    

:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:SETVARIABLES

if /I "%server%" == "apache" (

    (
    echo ^<VirtualHost *:80^>
    echo ServerName %project_url%
	    echo ServerAlias www.%project_url%
	    echo DocumentRoot "%route%\Pete\Public\suspend"
		echo.
	      echo ^<Directory "%route%\Pete\Public"^>              
              echo Require all granted
	      echo ^</Directory^>
		echo.
	echo ^</VirtualHost^>
    ) > "%apache_conf%\%project_name%.conf"
)


if /I "%debug%" == "active"	( goto debug)

:DEBUG
    echo project_name: %project_name%
    echo route: %route%
    echo apache_conf: %apache_conf%
    echo server: %server%
    echo server_version: %server_version%
    echo project_url: %project_url%
    echo os: %os%
    echo os_version: %os_version%
    echo debug: %debug%

