#!/bin/bash

while getopts v:n:u:r:a:x:j:t:w:k: option 
do 
case "${option}" 
	in 
	v) os=${OPTARG};;
	n) adomain_name=${OPTARG};; 
	u) site_url=${OPTARG};; 
	r) route=${OPTARG};; 
	a) apache_conf=${OPTARG};;
	x) filename=${OPTARG};;
	j) os_version=${OPTARG};;
	t) server=${OPTARG};;
	w) server_version=${OPTARG};;
	k) debug=${OPTARG};;
esac 
done 

#DEBUG
if test "$debug" = 'active'; then
	
echo os: $os
echo adomain_name: $adomain_name
echo site_url: $site_url
echo route: $route
echo apache_conf: $apache_conf
echo id: $id
echo filename: $filename
echo os_version: $os_version
echo server: $apache_version
echo debug: $debug

fi

if test "$server" = 'apache'; then
	rm -rf $apache_conf/$filename.conf
fi

