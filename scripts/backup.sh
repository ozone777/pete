#!/bin/bash

while getopts n:p:r:m:v:w:u:d:k:s: option 
do 
case "${option}" 
	in 
	n) project_name=${OPTARG};; 
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	m) mysql_bin=${OPTARG};;
	v) os=${OPTARG};;
	w) app_name=${OPTARG};;
	u) site_url=${OPTARG};;
	d) site_id=${OPTARG};;
	k) debug=${OPTARG};;
	s) schedulling=${OPTARG};;
esac 
done 

odb=`cd $route && grep "^define('DB_NAME'," $project_name/wp-config.php`
odb="${odb// /}"
odb=${odb#*,"'"}
odb=${odb%"'");}

if test "$debug" = 'active'; then

echo project_name: $project_name
echo db_root_pass: $db_root_pass
echo route: $route
echo mysql_bin: $mysql_bin
echo os: $os
echo app_name: $app_name
echo site_url: $site_url
echo backup_url: $backup_url
echo schedulling: $schedulling

fi

mkdir $route/Pete/operations/backups/$project_name$schedulling
backup_route=$route/Pete/operations/backups/$project_name$schedulling
mkdir $backup_route/massive_file

echo "domain: '$site_url'
platform: 'WordPress'
prefix: 'wp_'
" > $backup_route/massive_file/config.txt

if test "$db_root_pass" = 'none'; then
	$mysql_bin --host=localhost -uroot $odb > $backup_route/massive_file/query.sql
else
	$mysql_bin --host=localhost -uroot -p$db_root_pass $odb > $backup_route/massive_file/query.sql
fi

cp -r $route/$project_name $backup_route/massive_file/filem
chmod -R 755 $backup_route/massive_file
cd $backup_route && tar czf massive_file.tar.gz massive_file
mkdir $route/Pete/backups/$site_id
mv $backup_route/massive_file.tar.gz $route/Pete/backups/$site_id/$project_name-$schedulling.tar.gz

rm -rf $backup_route

