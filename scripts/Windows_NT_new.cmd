@echo off
setlocal enabledelayedexpansion
:ARGUMENTS_LOOP
if "%~1"==""  ( goto SETVARIABLES)
    if /I "%~1" == "-n" (set project_name=%~2& goto SHIFT)
    if /I "%~1" == "-u" (set project_url=%~2& goto SHIFT)
    if /I "%~1" == "-p" (set db_root_pass=%~2& goto SHIFT)
    if /I "%~1" == "-r" (set route=%~2& goto SHIFT)
    if /I "%~1" == "-m" (set mysql_bin=%~2& goto SHIFT)
    if /I "%~1" == "-a" (set apache_conf=%~2& goto SHIFT)
    if /I "%~1" == "-x" (set database_name=%~2& goto SHIFT)
    if /I "%~1" == "-y" (set database_user=%~2& goto SHIFT)
    if /I "%~1" == "-z" (set password_user=%~2& goto SHIFT)
    if /I "%~1" == "-b" (set wpkey1=%~2& goto SHIFT)
    if /I "%~1" == "-c" (set wpkey2=%~2& goto SHIFT)
    if /I "%~1" == "-d" (set wpkey3=%~2& goto SHIFT)
    if /I "%~1" == "-e" (set wpkey4=%~2& goto SHIFT)
    if /I "%~1" == "-f" (set wpkey5=%~2& goto SHIFT)
    if /I "%~1" == "-g" (set wpkey6=%~2& goto SHIFT)
    if /I "%~1" == "-h" (set wpkey7=%~2& goto SHIFT)
    if /I "%~1" == "-i" (set wpkey8=%~2& goto SHIFT)
    if /I "%~1" == "-v" (set os=%~2& goto SHIFT)
    if /I "%~1" == "-j" (set os_version=%~2& goto SHIFT)
    if /I "%~1" == "-t" (set server=%~2& goto SHIFT)
    if /I "%~1" == "-w" (set server_version=%~2& goto SHIFT)
    if /I "%~1" == "-k" (set debug=%~2& goto SHIFT)

:SHIFT
Shift
Shift
goto ARGUMENTS_LOOP

:SETVARIABLES

if /I "%db_root_pass%" == "none" (
 %mysql_bin% --host=localhost -uroot -e "create database %database_name%"
 %mysql_bin% --host=localhost -uroot -e "CREATE USER %database_user%@localhost"
 %mysql_bin% --host=localhost -uroot -e "SET PASSWORD FOR %database_user%@localhost = PASSWORD('%password_user%')"
 %mysql_bin% --host=localhost -uroot -e "GRANT ALL PRIVILEGES ON %database_name%.* TO %database_user%@localhost IDENTIFIED BY '%password_user%'"
) else (
 %mysql_bin% --host=localhost -uroot -p%db_root_pass% -e "create database %database_name%"
 %mysql_bin% --host=localhost -uroot -p%db_root_pass% -e "CREATE USER %database_user%@localhost"
 %mysql_bin% --host=localhost -uroot -p%db_root_pass% -e "SET PASSWORD FOR %database_user%@localhost = PASSWORD('%password_user%')"
 %mysql_bin% --host=localhost -uroot -p%db_root_pass% -e "GRANT ALL PRIVILEGES ON %database_name%.* TO %database_user%@localhost IDENTIFIED BY '%password_user%'"
)
set crud_route=%route%\Pete\operations\crud
rmdir /s /q "%crud_route%"
mkdir "%crud_route%"
copy "%route%\Pete\templates\wordpress_template.zip" "%crud_route%"
C:\WordPressPete\zip\7za.exe x "%crud_route%\wordpress_template.zip" -o%crud_route% 
::Call :UnZipFile "%crud_route%" "%crud_route%\wordpress_template.zip"

::NEXT2
cd %crud_route%
move %crud_route%\massive_file\filem %route%\%project_name%

(
echo define^('WP_HOME','http://%project_url%'^);
echo define^('WP_SITEURL','http://%project_url%'^);
echo define^('DB_NAME', '%database_name%'^);
echo define^('DB_USER', '%database_user%'^);
echo define^('DB_PASSWORD', '%password_user%'^);
echo.
echo define^('AUTH_KEY',         '%wpkey1%'^);
echo define^('SECURE_AUTH_KEY',  '%wpkey2%'^);
echo define^('LOGGED_IN_KEY',    '%wpkey3%'^);
echo define^('NONCE_KEY',        '%wpkey4%'^);
echo define^('AUTH_SALT',        '%wpkey5%'^);
echo define^('SECURE_AUTH_SALT', '%wpkey6%'^);
echo define^('LOGGED_IN_SALT',   '%wpkey7%'^);
echo define^('NONCE_SALT',       '%wpkey8%'^);
) > "%route%\otemp.txt"

copy "%route%\Pete\templates\w1_wp-template.php"+"%route%\otemp.txt"+"%route%\Pete\templates\w2_wp-template.php" "%route%/%project_name%/wp-config.php" /b
del "%route%\otemp.txt"

if /I "%server%" == "apache" (
::needs improvement
	mkdir C:\WordPressPete\Apache24\logs\%project_name%

    (
    echo ^<VirtualHost *:80^>
    echo ServerName %project_url%
	    echo ServerAlias www.%project_url%
	    echo DocumentRoot "%route%\%project_name%"
		echo.
	      echo ^<Directory "%route%\%project_name%"^>
              echo SetOutputFilter DEFLATE
              echo Options FollowSymLinks
              echo AllowOverride All
              echo Order Deny,Allow
              echo Require all granted
	      echo ^</Directory^>
		echo.
  	    echo ErrorLog "logs\%project_name%\error.log"
  	    echo CustomLog "logs\%project_name%\requests.log" combined
	echo ^</VirtualHost^>
    ) > "%apache_conf%\%project_name%.conf"

    ::Agrega al host del sistema
    echo.>> "C:\Windows\System32\drivers\etc\hosts"
    echo 127.0.0.1 %project_name%.test >> "C:\Windows\System32\drivers\etc\hosts"
    echo ::1 %project_name%.test >> "C:\Windows\System32\drivers\etc\hosts"

    ::Security .htaccess best practices
	del /f %route%\%project_name%\.htaccess
	copy %route%\Pete\templates\htaccess_main.txt %route%\%project_name%\.htaccess
	copy %route%\Pete\templates\htaccess_content.txt %route%\%project_name%\wp-content\uploads\.htaccess
)

if /I "%debug%" == "active"	( goto debug)

:DEBUG
   echo os: %os%
   echo project_name: %project_name%
   echo project_url:  %project_url%
   echo db_root_pass:  %db_root_pass%
   echo route:  %route%
   echo mysql_bin : %mysql_bin%
   echo apache_conf:  %apache_conf%
   echo database:  %database_name%
   echo user:  %database_user%
   echo password:  %password_user%
   echo wpkey1:  %wpkey1%
   echo wpkey2:  %wpkey2%
   echo wpkey3:  %wpkey3%
   echo wpkey4:  %wpkey4%
   echo wpkey5:  %wpkey5%
   echo wpkey6:  %wpkey6%
   echo wpkey7:  %wpkey7%
   echo wpkey8:  %wpkey8%
   echo os_version:  %os_version%
   echo server:  %server%
   echo server_version:  %server_version%
